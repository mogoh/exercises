#!/usr/bin/env python3

from random import choice
from random import choice, randint

from discord import Client
import tyche
from tyche import ar_letter, adjective, he_letter, letter, el_letter, eyecolor
from tyche import god, ruler, concern, gender, de_town, star, constellation
from tyche import color, haircolor, skincolor, ethnic_group, age, name, pornactor

bot = Client()


@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')


@bot.event
async def on_message(message):
    c = message.content
    cs = message.content.split()

    if c.startswith("!"):
        print(c)

    if "good bot" in c.lower() or "guter bot" in c.lower():
        await message.add_reaction('\N{GRINNING FACE WITH SMILING EYES}')
    if "bad bot" in c.lower() or "böser bot" in c.lower():
        await message.add_reaction('\N{POUTING FACE}')

    if cs[0] == '!hilfe' or cs[0] == '!help' or cs[0] == '!h':
        await message.channel.send(help(message))

    if cs[0] == '!adjektiv' or cs[0] == '!ad':
        await message.channel.send(adjective())
    if cs[0] == '!arabisch' or cs[0] == '!ar':
        await message.channel.send(ar_letter())
    if cs[0] == '!hebräisch' or cs[0] == '!he':
        await message.channel.send(he_letter())
    if cs[0] == '!buchstabe' or cs[0] == '!b':
        await message.channel.send(letter())
    if cs[0] == '!griechisch' or cs[0] == '!el':
        await message.channel.send(el_letter())
    if cs[0] == '!augenfarbe' or cs[0] == '!au':
        await message.channel.send(eyecolor())
    if cs[0] == '!gott' or cs[0] == '!go':
        await message.channel.send(god())
    if cs[0] == '!herrscher' or cs[0] == '!herr':
        await message.channel.send(ruler())
    if cs[0] == '!konzern' or cs[0] == '!k':
        await message.channel.send(concern())
    if cs[0] == '!geschlecht' or cs[0] == '!g':
        await message.channel.send(gender())
    if cs[0] == '!ort' or cs[0] == '!o':
        await message.channel.send(de_town())
    if cs[0] == '!stern' or cs[0] == '!s':
        await message.channel.send(star())
    if cs[0] == '!sternbild' or cs[0] == '!sb':
        await message.channel.send(constellation())
    if cs[0] == '!farbe':
        await message.channel.send(color())
    if cs[0] == '!haarfarbe' or cs[0] == '!haar':
        await message.channel.send(haircolor())
    if cs[0] == '!hautfarbe' or cs[0] == '!haut':
        await message.channel.send(skincolor())
    if cs[0] == '!ethnie' or cs[0] == '!e':
        await message.channel.send(ethnic_group())
    if cs[0] == '!alter':
        await message.channel.send(age())
    if cs[0] == '!name' or cs[0] == '!n':
        await message.channel.send(name(cs))
    if cs[0] == '!jain' or cs[0] == '!jn':
        await message.channel.send(choice(["Ja!", "Nein!"]))
    if cs[0] == '!wähle':
        if len(cs) >= 2:
            await message.channel.send(choose(message))
    if cs[0] == '!würfele' or cs[0] == '!w':
        if len(cs) >= 2:
            await message.channel.send(roll(message))
    if cs[0] == '!pornodarsteller' or cs[0] == '!porno' or cs[0] == '!p':
        await message.channel.send(pornactor())
    if cs[0] == '!heilpflanze' or cs[0] == '!heil':
        await message.channel.send(tyche.medicinal_herb())
    if cs[0] == '!edelstein' or cs[0] == '!ed':
        await message.channel.send(tyche.gem())


def choose(message):
    cs = message.content.split()
    return choice(cs[1:])


def roll(message):
    dice = message.content.split()
    try:
        return str(randint(1, int(dice[1])))
    except Exception:
        return 'Ich brauche eine Zahl.'


def help(message):
    cs = message.content.split()
    if "namen" in cs or "name" in cs or "n" in cs:
        help_name = """**!name** oder **!n** → Name
**!name alien** oder **!n a** → »Alien«-Name
**!name fantasy** oder **!n f** → »Fantasy«-Name
**!name de männlich** oder **!n de m** → In deutschland verbreiteter männlicher Name
**!name de weiblich** oder **!n de w** → In deutschland verbreiteter weiblicher Name
**!name de männlich beliebt** oder **!n de m b** → In deutschland beliebter männlicher Name
**!name de weiblich beliebt** oder **!n de w b** → In deutschland beliebter weiblicher Name
**!name de nachname** oder **!n de nn** → In deutschland verbreiteter Nachname
**!name de png männlich** oder **!n de png m** → Aus deutschen männlichen Namen generierter Fantasiname (Markovchain)
**!name de png weiblich** oder **!n de png w** → Aus deutschen weiblichen Namen generierter Fantasiname (Markovchain)
**!name en männlich** oder **!n de m** → Im englischsprachigen Raum verbreiteter männlicher Name
**!name en weiblich** oder **!n de w** → Im englischsprachigen Raum verbreiteter weiblicher Name
**!name en unisex** oder **!n en u** → Im englischsprachigen Raum mehrgeschlechtlicher Name
**!name en nachname** oder **!n en nn** → Im englischsprachigen Raum verbreiteter Nachname
**!name int unisex** oder **!n int u** → Internation verbreiteter mehrgeschlechtlicher Name
**!name alle** oder **!n a** → Irgendein Name
"""
        return help_name

    else:
        helptext = """Hallo,

ich bin Tyche, ein Discord-Bot, um dir bei deinen Schreibaktivitäten auf die Sprünge zu helfen. Sag mir, was du brauchst. Du kannst mich nach folgendem fragen:

**!name** oder **!n** → Name
Für mehr Optionen: »!hilfe name«

**!buchstabe** oder **!b** → Arabischer, Grichischer oder Hebräischer Buchstabe
**!arabisch** oder **!ar** → Arabischer Buchstabe
**!hebräisch** oder **!he** → Hebräischer Buchstabe
**!griechisch** oder **!el** → Grichischer Buchstabe

**!augenfarbe** oder **!au** → Augenfarbe
**!geschlecht** oder **!g** → Geschlecht
**!haarfarbe** oder **!haar** → Haarfarbe
**!hautfarbe** oder **!haut** → Hautfarbe
**!ethnie** oder **!e** → Ethnie (Global, gleichverteilt)
**!alter** → Alter (1 - 99)

**!adjektiv** oder **!ad** → Adjektiv
**!gott** oder **!go** → Gottheit
**!herrscher** oder **!herr** → Herrscher
**!konzern** oder **!k** → Konzern (Fiktiv)
**!ort** oder **!o** → Ort (aus Deutschland)
**!stern** oder **!s** → Stern
**!sternbild** oder **!sb** → Sternbild
**!farbe** → farbe
**!heilpflanze** oder **!heil** → Heilpflanze
**!edelstein** oder **!ed** → Edelstein

**!jain** oder **!jn** → »Ja« oder »Nein«?
**!wähle a b c** → »a«, »b« oder »c«?
**!würfele 6** oder **!w 6** → Eine Zahl zwischen 1 und 6

Ich liefer leider nicht immer gute Ergebnisse 😳, aber du kannst mich auch gerne noch ein zweites mal fragen. 😁 Bei Fragen und Anregungen, frag einfach @mogoh#2917. 😁
"""
        return helptext


bot.run('NDQ4NDQ3NzUyOTU1NzU2NTQ1.DeWRaQ.lxMQldGEqPBvHsEe_ltqfkARwBU')
