Starmapper
================================================================================
Some tools to map Stars. It uses CSV files in the Format of the hyg database.

Append & Randomnames
--------------------------------------------------------------------------------
Randomnames generates Starnames with two consonants and a number.
Append appends a greek, arabic or hebreic letter to a given name.

Distance
--------------------------------------------------------------------------------
Distance calculates the distance between two given Starnames.

Stargenerator
--------------------------------------------------------------------------------
Stargenerator generates Stars by given names.

Starmap
--------------------------------------------------------------------------------
Starmap is the main tool. It takes Stars an draws them on a SVG. It can also convert to PNG or PDF.

Requirements
--------------------------------------------------------------------------------
sudo apt-get install moreutils python3-pip inkscape
pip3 install svgwrite


sponge
sudo apt-get install moreutils

svgwrite
sudo apt-get install python3-pip
pip3 install svgwrite

Font:
Exo 2.0

inkscape
sudo apt-get install inkscape

