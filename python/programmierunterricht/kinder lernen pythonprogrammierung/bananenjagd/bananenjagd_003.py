# Wir importieren die relevanten Pygame-Module und Variablen:
import sys
import os
from random import randint
import pygame
from pygame.locals import *

# Initialisiere globale Variablen

# Wo befindet sich unser Python-Skript?
PFAD = os.path.dirname(os.path.realpath(__file__))

# Pygame muss zunächst initialisiert werden:
pygame.init()

# Bilder pro Sekunde
BPS = 60
# Die bpsUhr (Bilder pro Sekunde-Uhr) hilft uns dabei, dass ein Programm nicht 
# schneller läuft, als es sollte. Wir brauchen die bpsUhr später. 30 BPS
# bedeutet alle 1/60 = 0,01666... Sekunden erzeugen wir ein neues Bild.
bpsUhr = pygame.time.Clock()

# set up the window
BREITE = 800
HÖHE = 800
ANZEIGEFLÄCHE = pygame.display.set_mode((BREITE, HÖHE), 0, 32)
pygame.display.set_caption('Bananenjagd')

# Farbe weiß definiert als ein 3-Tupel
#        rot grün blau
WEISS = (255, 255, 255)
ROT   = (255,   0,   0)

# Richtungen
HOCH = 'hoch'
RUNTER = 'runter'
LINKS = 'links'
RECHTS = 'rechts'
HALT = 'halt'


class Objekt:
    bild = None
    breite = None
    höhe = None
    x = None
    y = None
    richtung = None
    geschwindigkeit = None


def initialisiere_spiel():
    # Wir laden ein Hintergrundbild.
    bild_pfad = os.path.join(PFAD, 'hintergrund.png')
    hintergrundbild = pygame.image.load(bild_pfad)

    banane = Objekt()
    # Wir laden das Bild einer Banane:
    bild_pfad = os.path.join(PFAD, 'banane.png')
    banane.bild = pygame.image.load(bild_pfad)
    # Größe:
    banane.breite = 17
    banane.höhe = 15
    # Startposition der Banane
    banane.x = randint(10, BREITE - banane.breite - 11)
    banane.y = randint(10, HÖHE - banane.höhe - 11)

    katze = Objekt()
    # Wir laden das Bild einer Katze
    bild_pfad = os.path.join(PFAD, 'katze.png')
    katze.bild = pygame.image.load(bild_pfad)
    # Größe:
    katze.breite = 115
    katze.höhe = 75

    # Startposition der Katze
    katze.x = randint(0, BREITE - katze.breite - 1)
    katze.y = randint(0, HÖHE - katze.höhe - 1)

    # Richtung gibt an, in welche Richtung sich die Katze initial bewegen soll.
    katze.richtung = HALT
    # Wie schnell bewegt sich die Katze?
    katze.geschwindigkeit = 1
    return (hintergrundbild, katze, banane)


def beende_spiel():
    pygame.quit()
    sys.exit()


def prüfe_überdeckung(a, b):
    # Wenn die Katze die Banane überdeckt:
    if (b.x >= a.x and
            b.x + b.breite <= a.x + a.breite and
            b.y >= a.y and
            b.y + b.höhe <= a.y + a.höhe):
        return True
    else:
        return False


def ereigniss(katze):
    # Wir überprüfen die Eingabe:
    for ereignis in pygame.event.get():
        # Wenn der Nutzer das Programm beenden möchte:
        if ereignis.type == QUIT:
            beende_spiel()
        # Wir Prüfen, ob eine Taste gedrückt wurde:
        if ereignis.type == KEYDOWN:
            if ereignis.key == K_LEFT:
                katze.richtung = LINKS
            elif ereignis.key == K_RIGHT:
                katze.richtung = RECHTS
            elif ereignis.key == K_UP:
                katze.richtung = HOCH
            elif ereignis.key == K_DOWN:
                katze.richtung = RUNTER
            elif ereignis.key == K_ESCAPE:
                beende_spiel()


def bewege(katze, banane):
    # Wir bewegen die Katze:
    if katze.richtung == RECHTS:
        katze.x += katze.geschwindigkeit
    if katze.richtung == RUNTER:
        katze.y += katze.geschwindigkeit
    if katze.richtung == LINKS:
        katze.x -= katze.geschwindigkeit
    if katze.richtung == HOCH:
        katze.y -= katze.geschwindigkeit

    # Wenn die Katze die Banane überdeckt:
    if prüfe_überdeckung(katze, banane):
        # Erhöhe die Geschwindigkeit:
        katze.geschwindigkeit += 1
        # Bewege die Banane zu einer neuen Position:
        banane.x = randint(10, BREITE - banane.breite - 11)
        banane.y = randint(10, HÖHE - banane.höhe - 11)

    # Wenn die Katze den Rand berührt:
    if (katze.x < 0 or
            katze.x + katze.breite > BREITE or
            katze.y < 0 or
            katze.y + katze.höhe > HÖHE):
        print("Ende")
        print("Punkte: " + str(katze.geschwindigkeit - 1))
        beende_spiel()


def zeichne_gitter():
    gittergröße = 80
    gitterfarbe = ROT
    spalten = 10
    reihen = 10
    for x in range(spalten + 1):
        pygame.draw.line(
            ANZEIGEFLÄCHE,
            gitterfarbe,
            (x * gittergröße - 1, 0), # Startpunkt
            (x * gittergröße - 1, HÖHE), # Endpunkt
            2, # Liniendicke
        )
    for y in range(reihen + 1):
        pygame.draw.line(
            ANZEIGEFLÄCHE,
            gitterfarbe,
            (0, y * gittergröße - 1), # Startpunkt
            (BREITE, y * gittergröße - 1), # Endpunkt
            2, # Liniendicke
        )


def zeichnen(hintergrundbild, katze, banane):
    # Wir füllen die Anzeigefläche mit weißer Farbe:
    ANZEIGEFLÄCHE.fill(WEISS)

    # blit: Bit Block Image Transfer - Bewege ein Bild (auf eine für den
    # Computer einfache Weise).
    # Zeichne den Hintergrund.
    ANZEIGEFLÄCHE.blit(hintergrundbild, (0, 0))

 

    # Zeichne die Banane:
    ANZEIGEFLÄCHE.blit(banane.bild, (banane.x, banane.y))

    # Zeichne die Kazte:
    ANZEIGEFLÄCHE.blit(katze.bild, (katze.x, katze.y))

    # Wir haben die Anzeigefläche bearbeitet. Nun muss der Bildschirm
    # akualisiert werden.
    pygame.display.update()


def main():
    # Um Variablen zu verändern, die außerhal der Funktion definiert wurden,
    # müssen diese als "global" gekennzeichnet werden.

    hintergrundbild, katze, banane = initialisiere_spiel()

    # Haupt-Spiel-Schleife
    while True:
        ereigniss(katze)
        bewege(katze, banane)
        zeichnen(hintergrundbild, katze, banane)

        # Jetzt müssen wir warten, bis genügend Zeit vergangen ist.
        bpsUhr.tick(BPS)


if __name__ == '__main__':
    main()
