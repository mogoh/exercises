#!/usr/bin/env python3

from random import choice, randint
from sys import argv
from glob import glob


def f2sl(files):
    """
    Files to String-List
    """
    data = []

    # open files
    if type(files) == list:
        for s in files:
            with open(s, "r") as f:
                data += f.read().split("\n")
            f.close()
    elif type(files) == str:
        with open(files, "r") as f:
            data += f.read().split("\n")
        f.close()
    else:
        return

    # filter empty
    data = list(filter(None, data))

    # make things unique
    data = sorted(data)
    data = set(data)
    data = list(data)

    return data


def letter():
    alphabets = [
        "griechisches_alphabet.txt",
        "arabisches_alphabet.txt",
        "hebräisches_alphabet.txt",
    ]
    return choice(f2sl(alphabets))


def el_letter():
    el_alphabet = ["griechisches_alphabet.txt", ]
    return choice(f2sl(el_alphabet))


def ar_letter():
    ar_alphabet = ["arabisches_alphabet.txt", ]
    return choice(f2sl(ar_alphabet))


def he_letter():
    he_alphabet = ["hebräisches_alphabet.txt", ]
    return choice(f2sl(he_alphabet))

################################################################################


def adjective():
    de_adjective = ["deutsch/adjektive.txt", ]
    return choice(f2sl(de_adjective))


def eyecolor():
    augenfarbe = ["deutsch/augenfarben.txt", ]
    return choice(f2sl(augenfarbe))


def pornactor():
    pornactor = ["pornodarsteller.txt", ]
    return choice(f2sl(pornactor))


def god():
    god = "gottheiten.txt"
    return choice(f2sl(god))


def ruler():
    ruler = ["herrscher.txt"]
    return choice(f2sl(ruler))


def concern():
    concern = ["konzerne.txt", ]
    return letter() + " " + choice(f2sl(concern))


def star():
    star = ["sterne.txt", ]
    return choice(f2sl(star))


def constellation():
    constellation = ["deutsch/sternenbilder.txt", ]
    return choice(f2sl(constellation))


def de_town():
    town = ["deutsch/orte.txt", ]
    return choice(f2sl(town))


def gender():
    return choice(["männlich", "weiblich"])


def skincolor():
    skincolor = ["deutsch/hautfarben.txt"]
    return choice(f2sl(skincolor))


def color():
    color = ["deutsch/farben.txt"]
    return choice(f2sl(color))


def haircolor():
    haircolor = ["deutsch/haarfarben.txt"]
    return choice(f2sl(haircolor))


def age():
    return randint(1, 99)


def ethnic_group():
    ethnic = glob("ethnie*.txt")
    return choice(f2sl(ethnic))


def medicinal_herb():
    return choice(f2sl("deutsch/heilpflanzen.txt"))


def gem():
    return choice(f2sl("deutsch/edelstein.txt"))

################################################################################


def name(argv):
    if "alien" in argv or "a" in argv:
        alien = ["namen/alien.txt", ]
        return choice(f2sl(alien))
    elif "fantasy" in argv or "f" in argv:
        fantasy = ["namen/fantasy.txt", ]
        return choice(f2sl(fantasy))
    elif "de" in argv:
        if "beliebt" in argv or "b" in argv:
            if "männlich" in argv or "m" in argv:
                return choice(f2sl("namen/de_m_beliebt.txt"))
            elif "weiblich" in argv or "w" in argv:
                return choice(f2sl("namen/de_w_beliebt.txt"))
            else:
                return "Unmögliche Eingabe."
        elif "png" in argv:
            if "männlich" in argv or "m" in argv:
                return choice(f2sl("namen/de_png_m.txt"))
            elif "weiblich" in argv or "w" in argv:
                return choice(f2sl("namen/de_png_w.txt"))
            elif "mix" in argv:
                return choice(f2sl("namen/de_png_mix.txt"))
            else:
                return "Unmögliche Eingabe."
        elif "männlich" in argv or "m" in argv:
            return choice(f2sl("namen/de_m.txt"))
        elif "weiblich" in argv or "w" in argv:
            return choice(f2sl("namen/de_w.txt"))
        elif "nachname" in argv or "nn" in argv:
            return choice(f2sl("namen/de_nachnamen.txt"))
        else:
            return "Unmögliche Eingabe."
    elif "en" in argv:
        if "männlich" in argv or "m" in argv:
            return choice(f2sl("namen/en_m.txt"))
        elif "weiblich" in argv or "w" in argv:
            return choice(f2sl("namen/en_w.txt"))
        elif "unisex" in argv or "u" in argv:
            return choice(f2sl("namen/en_unisex.txt"))
        elif "nachname" in argv or "nn" in argv:
            return choice(f2sl("namen/en_nachnamen.txt"))
        else:
            return "Unmögliche Eingabe."
    elif "int" in argv:
        if "unisex" in argv or "u" in argv:
            return choice(f2sl("namen/int_unisex.txt"))
    elif "alle" in argv or "a" in argv:
        name = glob("namen/*")
        return choice(f2sl(name))
    else:
        return "Unmögliche Eingabe."


def main():
    if len(argv) > 1:
        if argv[1] == "arabisch" or argv[1] == "ar":
            print(ar_letter())
        elif argv[1] == "griechisch" or argv[1] == "el":
            print(el_letter())
        elif argv[1] == "hebräisch" or argv[1] == "he":
            print(he_letter())
        elif argv[1] == "adjektiv" or argv[1] == "ad":
            print(adjective())
        elif argv[1] == "buchstabe" or argv[1] == "b":
            print(letter())

        elif argv[1] == "augenfarbe" or argv[1] == "au":
            print(eyecolor())
        elif argv[1] == "pornodarsteller" or argv[1] == "p":
            print(pornactor())
        elif argv[1] == "gott" or argv[1] == "go":
            print(god())
        elif argv[1] == "herrscher" or argv[1] == "herr":
            print(ruler())
        elif argv[1] == "konzern" or argv[1] == "k":
            print(concern())
        elif argv[1] == "geschlecht" or argv[1] == "ge":
            print(gender())
        elif argv[1] == "ort" or argv[1] == "o":
            print(de_town())
        elif argv[1] == "stern" or argv[1] == "s":
            print(star())
        elif argv[1] == "sternbild" or argv[1] == "sb":
            print(constellation())
        elif argv[1] == "farbe" or argv[1] == "far":
            print(color())
        elif argv[1] == "haarfarbe" or argv[1] == "haar":
            print(haircolor())
        elif argv[1] == "hautfarbe" or argv[1] == "haut":
            print(skincolor())
        elif argv[1] == "ethnie" or argv[1] == "et":
            print(ethnic_group())
        elif argv[1] == "alter":
            print(age())
        elif argv[1] == "heilpflanze" or argv[1] == "heil":
            print(medicinal_herb())
        elif argv[1] == "edelstein" or argv[1] == "ed":
            print(gem())

        elif argv[1] == "name" or argv[1] == "n":
            print(name(argv))
        else:
            print("Wrong parameter!")


if __name__ == "__main__":
    main()
