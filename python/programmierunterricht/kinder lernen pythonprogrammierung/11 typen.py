# In python gibt es verschiedene Datentypen.
# Wir haben schon verschiedene kenne gelernt aber noch nie explizeit darüber gesprochen.
# Dies wollen wir jetzt ändern.
# Zum einen gibt es die ganzen Zahlen, wie zum Beispiel 2, 5432, 123.

# Zahlen können wir wie gewohnt addieren.

print(2+3)


# Dann gibt es die Zeichenketten.

print("Hallo")


# Auch Zeichenketten können wir addieren. Das ist zwar keine Mathematik, aber Python erlaubt es.

print("Hallo" + "Welt!")

# Aber wie können nicht Zeichenketten und Zahlen addieren! Denn beides sind verschiedene Typen.

print("Hallo" + 2)


# Python unterscheidet zwischen Ganzzahlen und Kommazahlen.
# In python wird ein Punk an Stelle eines Kommas verwendet (das kommt aus dem englischen)

print(3.5 * 2)

# Ganzzahlen werden dabei in Kommazahlen umgewandelt.