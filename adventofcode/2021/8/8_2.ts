const INPUT_FILE = "./input8.txt";
// const INPUT_FILE = "./input8_test.txt";

function isSuperset(superset: Set<any>, subset: Set<any>): boolean {
    for (let elem of subset) {
        if (!superset.has(elem)) {
            return false;
        }
    }
    return true;
}

function isEqual(set1: Set<any>, set2: Set<any>): boolean {
    if (set1.size !== set2.size) {
        return false;
    } else if (!isSuperset(set1, set2)) {
        return false;
    } else if (!isSuperset(set2, set1)) {
        return false;
    } else {
        return true;
    }
}

function digit_array_to_number(digits: number[]): number {
    let result = 0;
    for (let i = 0; i < digits.length; i++) {
        result += digits[i] * Math.pow(10, digits.length - i - 1);
    }
    return result;
}

function create_decoder_table(encoded: Set<string>[]): Set<string>[] {
    let decoder_table: Set<string>[] = new Array(10).fill(new Set<string>());

    let size_five: Set<string>[] = [];
    let size_six: Set<string>[] = [];

    encoded.forEach(s => {
        if (s.size === 2) {
            decoder_table[1] = s;
        } else if (s.size === 3) {
            decoder_table[7] = s;
        } else if (s.size === 4) {
            decoder_table[4] = s;
        } else if (s.size === 5) {
            size_five.push(s);
        } else if (s.size === 6) {
            size_six.push(s);
        } else if (s.size === 7) {
            decoder_table[8] = s;
        }
    });

    // 9
    for (let i in size_six) {
        if (isSuperset(size_six[i], decoder_table[4])) {
            decoder_table[9] = size_six[i];
            size_six.splice(parseInt(i), 1);
            break;
        }
    }

    // 0

    for (let i in size_six) {
        if (isSuperset(size_six[i], decoder_table[1])) {
            decoder_table[0] = size_six[i];
            size_six.splice(parseInt(i), 1);
            break;
        }
    }

    // 6
    decoder_table[6] = size_six[0];

    // 3
    for (let i in size_five) {
        if (isSuperset(size_five[i], decoder_table[1])) {
            decoder_table[3] = size_five[i];
            size_five.splice(parseInt(i), 1);
            break;
        }
    }

    // 5
    for (let i in size_five) {
        if (isSuperset(decoder_table[6], size_five[i])) {
            decoder_table[5] = size_five[i];
            size_five.splice(parseInt(i), 1);
            break;
        }
    }

    // 2
    decoder_table[2] = size_five[0];

    return decoder_table;
}

function decode_digit(digit: Set<string>, decoder_table: Set<string>[]): number {
    for (let i = 0; i < 10; i++) {
        if (isEqual(decoder_table[i], digit)) {
            return i;
        }
    }
    return -1;
}

const text: string = await Deno.readTextFile(INPUT_FILE);
const ss_encoded: Set<string>[][][] =
    text.split("\n")
        .map(
            s => s.split(" | ")
                .map(
                    s => s.split(" ")
                        .map(s => new Set(s))
                )
        );

const decoder_list: Set<string>[][] = [];

let sum = 0;

for (let ss of ss_encoded) {
    let decoder_table = create_decoder_table(ss[0])
    let result: number[] = [];
    for (let n of ss[1]) {
        result.push(decode_digit(n, decoder_table));
    }
    sum += digit_array_to_number(result);
}

console.log(sum);
