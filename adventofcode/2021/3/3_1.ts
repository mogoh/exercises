const INPUT_FILE = "./input3.txt";
// 12 figures

const text = await Deno.readTextFile(INPUT_FILE);

const report: number[] = text.split("\n").map(
    line => parseInt(line, 2)
);

let counter = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
report.forEach(reading => {
    for (let i = 0; i < 12; i += 1) {
        if (reading & (1 << i)) {
            counter[i] += 1;
        }
    }
});

let gamma = 0;
let epsilon = 0;
for (let i = 0; i < 12; i += 1) {
    if (counter[i] > 500) {
        gamma += 2 ** i;
    } else {
        epsilon += 2 ** i;
    }
}

const power_consumption = gamma * epsilon;

console.log(power_consumption);
