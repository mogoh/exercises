type Attributes = Array<[string, string]>

export class Ultra {
    tag: string;
    attributes: Attributes | undefined;
    children: (Ultra | string)[] | undefined;

    constructor(tag: string, attributes: Attributes, children: (Ultra | string)[]) {
        this.tag = tag;
        this.attributes = attributes;
        this.children = children;
    }

    render(): HTMLElement {
        const element = document.createElement(this.tag);
        if (this.attributes !== undefined) {
            for (const [k, v] of this.attributes) {
                element.setAttribute(k, v);
            }
        }
        this.children?.map(child => {
            if (typeof (child) === "string") {
                return child;
            } else {
                return child.render();
            }
        }).forEach(child => {
            element.append(child);
        });

        return element;
    }
}

// Factory
export function t(tag: string): Ultra {
    return new Ultra(tag, [], []);
}

export function ta(tag: string, attributes: Attributes): Ultra {
    return new Ultra(tag, attributes, []);
}

export function tc(tag: string, children: (Ultra | string)[]): Ultra {
    return new Ultra(tag, [], children);
}

export function tac(tag: string, attributes: Attributes, children: (Ultra | string)[]): Ultra {
    return new Ultra(tag, attributes, children);
}
