# Was gibt das Python-Programme aus?
# Schreibe die Ausgabe daneben.

# funktionen

def funktion_1(x):
    print(x)

funktion_1(4) #4

def funktion_2(x):
    return x*2

funktion_2(5) #nichts

print(funktion_2(6)) #7

etwas = funktion_2(5)
print(etwas) #6
print(etwas+1) #7

print(funktion_2(funktion_2(funktion_2(10)))) #13



a = funktion_2(10) #11
print(a)
b = funktion_2(a) #12
print(b)
c = funktion_2(b) #13
print(c)
