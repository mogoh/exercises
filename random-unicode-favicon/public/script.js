const LAST_UNICODE = 0x3134f;

function get_favicon() {
    let link = document.querySelector("link[rel~='icon']");
    if (link === undefined || link === null) {
        link = document.createElement("link");
        link.rel = "icon";
        document.getElementsByTagName("head")[0].appendChild(link);
        return link;
    } else {
        return link;
    }
}

function set_favicon() {
    let link = get_favicon();
    let char = String.fromCodePoint(Math.floor(Math.random() * LAST_UNICODE));
    let svg_data_uri = `data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3E%3Ctext x='0' y='14'%3E${char}%3C/text%3E%3C/svg%3E`;
    link.href = svg_data_uri;
}

set_favicon();
