'''
Schreibe ein Programm das eine Liste der Zahlen von 1 bis 100 erzeugt.

Tipps:
* Erstelle eine leere Liste.
* Nutze eine while-Schleife.
* Füge Element mit der append-Funktion an.
'''

liste = []
i = 1
while i <= 100:
    liste.append(i)
    i += 1

print(liste)

