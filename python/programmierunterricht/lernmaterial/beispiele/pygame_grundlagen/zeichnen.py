# Wir importieren die relevanten Pygame-Module und Variablen:
import pygame, sys
from pygame.locals import *

# Pygame muss initialisiert werden:
pygame.init()

# Wir erstellen ein neues Fenster der größe 400×300 pixel
ANZEIGEFLAECHE = pygame.display.set_mode((400, 300), 0, 32)

# Der Titel des Fensters:
pygame.display.set_caption('Zeichnen')

# Wir benennen Farben
SCHWARZ = (  0,   0,   0)
WEISS   = (255, 255, 255)
ROT     = (255,   0,   0)
GRUEN   = (  0, 255,   0)
BLAU    = (  0,   0, 255)

# Wir zeichnen auf der Anzeigefläche
# Wir Füllen die Fläche weiß
ANZEIGEFLAECHE.fill(WEISS)
# Wir zeichnen ein Fünf-Eck
pygame.draw.polygon(ANZEIGEFLAECHE, GRUEN, 
                    ((146, 0), (291, 106), (236, 277), (56, 277), (0, 106))
                   )
# Wir zeichnen drei Linien
pygame.draw.line(ANZEIGEFLAECHE, BLAU, (60, 60), (120, 60), 4)
pygame.draw.line(ANZEIGEFLAECHE, BLAU, (120, 60), (60, 120))
pygame.draw.line(ANZEIGEFLAECHE, BLAU, (60, 120), (120, 120), 4)
# Wir zeichnen einen Kreis
pygame.draw.circle(ANZEIGEFLAECHE, BLAU, (300, 50), 20, 0)
# Wir zeichnen eine Ellipse
pygame.draw.ellipse(ANZEIGEFLAECHE, ROT, (300, 200, 40, 80), 1)
# Wir zeichnen ein Rechteck
pygame.draw.rect(ANZEIGEFLAECHE, ROT, (200, 150, 100, 50))

# Wir zeichnen einzelne Pixel
pixObj = pygame.PixelArray(ANZEIGEFLAECHE)
pixObj[380][280] = SCHWARZ
pixObj[382][282] = SCHWARZ
pixObj[384][284] = SCHWARZ
pixObj[386][286] = SCHWARZ
pixObj[388][288] = SCHWARZ
del pixObj

# Haupt-Spiel-Schleife
while True:
    # Wir arbeiten alle Ereignisse ab (Mausklicks, Tastendrücken, ...)
    for ereigniss in pygame.event.get():
        # Ist eine Ereigniss vom Typ »Beenden«?
        if ereigniss.type == QUIT:
            # Beende Pygame
            pygame.quit()
            # Beende das Programm
            sys.exit()
    # Wir zeichnen die Anzeigefläche neu
    pygame.display.update()

