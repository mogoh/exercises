const INPUT_FILE = "./input11.txt";
// const INPUT_FILE = "./input11_test.txt";
const TEXT: string = await Deno.readTextFile(INPUT_FILE);

class Task11 {
    grid: number[][] = [];
    flash_counter = 0;

    constructor() {
        this.parse();
    }

    traverse(f: (x: number, y: number, line: number[], cell: number) => void) {
        this.grid.forEach((line, i) => {
            line.forEach((cell, j) => {
                f(i, j, line, cell);
            });
        });
    }

    print_grid() {
        console.log('----------');
        this.grid.forEach((line) => {
            console.log(line.join(""));
        });
        console.log('----------');
    }

    parse() {
        this.grid = TEXT.split("\n").map((line) => {
            return line.split("").map((c) => {
                return Number(c);
            });
        });
    }

    increase_energy() {
        this.traverse((i, j) => {
            this.grid[i][j] += 1;
        });
    }

    get_neighbours(x: number, y: number): number[][] {
        const neighbours: number[][] = [];

        for (let i = x - 1; i <= x + 1; i += 1) {
            for (let j = y - 1; j <= y + 1; j += 1) {
                if (
                    i >= 0 &&
                    i <= 9 &&
                    j >= 0 &&
                    j <= 9 &&
                    (i != x ||
                    j != y)
                ) {
                    neighbours.push([i, j]);
                }
            }
        }
        return neighbours;
    }

    flash() {
        this.traverse((x, y, line, cell) => {
            if (cell > 9 && cell < 100) {
                const neighbours = this.get_neighbours(x, y);
                this.flash_counter += 1;
                this.grid[x][y] = 100;
                neighbours.forEach(([i, j]) => {
                    this.grid[i][j] += 1;
                });
            }
        });
    }

    reset_energe() {
        this.traverse((x, y, line, cell) => {
            if (cell > 9) {
                this.grid[x][y] = 0;
            }
        });
    }

    step() {
        this.increase_energy();
        let temp_flash_counter = this.flash_counter;
        do {
            temp_flash_counter = this.flash_counter;
            this.flash();
        } while (temp_flash_counter != this.flash_counter);
        this.reset_energe();

        this.print_grid();
        console.log("Flashes:", this.flash_counter);
        console.log();
    }

    run() {
        console.log("Task 11");
        this.print_grid();
        for (let i = 1; i <= 100; i += 1) {
            console.log("Step:", i);
            this.step();
        }
    }
}

const task11 = new Task11();
task11.run();
