# Listen speichern Sequenzen


ziffern = [0,1,2,3,4,5,6,7,8,9]

buchstaben = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]


# Leere Liste:
liste = []


# Wir können mit einer bereits gefüllten Liste anfangen
andere_liste = [4, 5, 6]


# append fügt Daten am Ende der Liste ein
liste.append(1)    #liste ist jetzt [1]
liste.append(2)    #liste ist jetzt [1, 2]
liste.append(4)    #liste ist jetzt [1, 2, 4]
liste.append(3)    #liste ist jetzt [1, 2, 4, 3]


# Vom Ende der Liste mit pop entfernen
liste.pop()        #=> 3 und liste ist jetzt [1, 2, 4]


# und dann wieder hinzufügen
liste.append(3)    # li ist jetzt wieder [1, 2, 4, 3].

# Greife auf Listen per Index zu
liste[0] #=> 1


# Das letzte Element ansehen
liste[-1] #=> 3

# Bei Zugriffen außerhalb der Liste kommt es jedoch zu einem IndexError
liste[4] # Verursacht einen IndexError

# Wir können uns Abschnitte so ansehen
liste[1:3] #=> [2, 4]

# Den Anfang auslassen
liste[2:] #=> [4, 3]

# Das Ende auslassen
liste[:3] #=> [1, 2, 4]

# Jeden Zweiten Eintrag auswählen
liste[::2]   # =>[1, 4]

# Eine umgekehrte Kopie zurückgeben
liste[::-1]   # => [3, 4, 2, 1]


# Jegliche Kombination dieser Syntax machen fortgeschrittene Slices möglich
# liste[Start:Ende:Schritt]

# Ein bestimmtes Element mit del aus der Liste entfernen
del liste[2] # li ist jetzt [1, 2, 3]

# Listen können addiert werden
liste + andere_liste #=> [1, 2, 3, 4, 5, 6] - Hinweis: liste und andere_liste werden in Ruhe gelassen

# Listen mit extend verknüpfen
liste.extend(andere_liste) # Jetzt ist li [1, 2, 3, 4, 5, 6]

# Mit in auf Existenz eines Elements prüfen
1 in liste #=> True

# Die Länge der Liste mit len ermitteln
len(liste) #=> 6
