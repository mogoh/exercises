# Arithmetik
# Wie wir mit Python rechnen.

1 + 1 # => 2
8 - 1 # => 7
10 * 2 # => 20

# Außer Division, welche automatisch Gleitkommazahlen zurückgibt
35 / 5  # => 7.0

# Eine Division kann mit "//" für positive sowie negative Werte abgerundet werden.
5 // 3     # => 1
5.0 // 3.0 # => 1.0 # works on floats too
-5 // 3  # => -2
-5.0 // 3.0 # => -2.0

# Benutzt man eine Gleitkommazahl, ist auch das Ergebnis eine solche
3 * 2.0 # => 6.0

# Der Rest einer Division
7 % 3 # => 1

# Potenz
2**4 # => 16

# Rangfolge wird mit Klammern erzwungen
(1 + 3) * 2 # => 8
