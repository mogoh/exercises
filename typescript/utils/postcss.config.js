const postcssPresetEnv = require('postcss-preset-env');
const postcssImport = require('postcss-import');
const importUrl = require('postcss-import-url');
const cssnano = require('cssnano');
const url = require("postcss-url");


const isProduction = process.env.NODE_ENV === 'production'


const dev = {
    plugins: [
        importUrl(),
        postcssImport({
            path: 'src/css',
        }),
        postcssPresetEnv({
            stage: 0,
        }),
        url({
            url: 'copy',
            assetsPath: './temp/',
        }),
    ],
}

const prod = {
    plugins: [
        importUrl(),
        postcssImport({
            path: 'src/css',
        }),
        postcssPresetEnv({
            stage: 0,
        }),
        cssnano({
            preset: 'default',
        }),
        url({
            url: 'copy',
            assetsPath: './public/css/',
        }),
    ],
}

module.exports =  isProduction
    ? prod
    : dev;
