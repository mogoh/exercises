# Wir importieren die relevanten Pygame-Module und Variablen:
import sys
import os
import pygame
from pygame.locals import QUIT

# Wo befindet sich unser Python-Skript?
PFAD = os.path.dirname(os.path.realpath(__file__))

# Pygame muss zunächst initialisiert werden:
pygame.init()

# Bilder pro Sekunde
BPS = 60
# Die bpsUhr (Bilder pro Sekunde-Uhr) hilft uns dabei, dass ein Programm nicht 
# schneller läuft, als es sollte. Wir brauchen die bpsUhr später. 30 BPS
# bedeutet alle 1/30 = 0,01666... Sekunden erzeugen wir ein neues Bild. 
bpsUhr = pygame.time.Clock()

# set up the window
ANZEIGEFLÄCHE = pygame.display.set_mode((400, 300), 0, 32)
pygame.display.set_caption('Animation')

# Farbe weiß definiert als ein 3-Tupel
WEISS = (255, 255, 255)
#hhg<fxvchh<fx
hintergrundbild_pfad = os.path.join(










# Haupt-Spiel-Schleife
while True:
    # Wir füllen die Anzeigefläche mit weißer Farbe:
    ANZEIGEFLÄCHE.fill(WEISS)

    for ereignis in pygame.event.get():
        # Wenn der Nutzer das Programm beenden möchte:
        if ereignis.type == QUIT:
            pygame.quit()
            sys.exit()

    # Wir haben die Anzeigefläche bearbeitet. Nun muss der Bildschirm 
    # akualisiert werden.
    pygame.display.update()
    # Jetzt müssen wir warten, bis genügend Zeit vergangen ist.
    bpsUhr.tick(BPS)