# Was gibt das Python-Programme aus?
# Schreibe die Ausgabe daneben.

if True:
    print("Ja")

if False:
    print("Ja")
else:
    print("Nein")

x = 5

if x>5:
    print("görßer")
elif x<5:
    print("kleiner")
else:
    print("gleich")

def eine_funktion(x):
    if x<1000:
        return True
    else:
        return False

if eine_funktion(123):
    print("Zahl kleiner 1000.")

