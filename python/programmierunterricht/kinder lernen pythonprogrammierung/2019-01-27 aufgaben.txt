Aufgaben 1:

Schreibe ein Programm das eine Liste der Zahlen von 1 bis 100 erzeugt.

Tipps:
* Erstelle eine leere Liste.
* Nutze eine while-Schleife.
* Füge Element mit der append-Funktion an.


Aufgabe 2:

Schreibe ein Programm, dass alle Kombinationen der Zahlen von 1 bis 10 ausgibt.

1 1
1 2
1 3
1 4
1 5
1 6
1 7
1 8
1 9
1 10
2 1
2 2
2 3
2 4
2 5
2 6
2 7
2 8
2 9
2 10
3 1
3 2
3 3
und so weiter ...

Tipps:
* Nutze zwei ineinander verschachtelte while-Schleifen.
* Nutze zwei Variablen



Aufgabe 3:

Schreibe ein Programm, dass die Zahlen von 1 bis 20 jeweils miteinander multipliziert und ausgibt.

1*1 = 1
1*2 = 2
1*3 = 3
1*4 = 4
1*5 = 5
1*6 = 6
1*7 = 7
1*8 = 8
1*9 = 9
1*10 = 10
1*11 = 11
1*12 = 12
1*13 = 13
1*14 = 14
1*15 = 15
1*16 = 16
1*17 = 17
1*18 = 18
1*19 = 19
1*20 = 20
2*1 = 2
2*2 = 4
2*3 = 6
2*4 = 8
2*5 = 10
2*6 = 12
2*7 = 14
2*8 = 16
2*9 = 18
2*10 = 20
2*11 = 22
2*12 = 24
2*13 = 26
2*14 = 28
2*15 = 30
2*16 = 32
2*17 = 34
2*18 = 36
2*19 = 38
2*20 = 40
3*1 = 3
3*2 = 6
3*3 = 9
und so weiter ...

Tipps:
* Du kannst das Programm von Aufgabe 2 nutzen und musst es nur leicht abwandeln.


Aufgabe 4:

Die Fibonacci-Folge ist eine Folge von Zahlen, bei der eine Zahl immer die Summe der beiden Vorgänger ist.
Die ersten Zahlen sind:
1, 1, 2, 3, 5, 8, 13, 21, und so weiter ...

Programmiere ein Programm, dass die ersten 100 Zahlen ausgibt.

Tipps:
* Es genügt eine While-Scheilfe um das Problem zu lösen.
* Diese Aufgabe ist etwas kniffeliger, aber mit ein bisschen Geduld werden wir zur Lösung kommen.


Aufgabe 5:

Zeichenketten werden in Python wie Listen behandelt.
Schreibe ein Programm, dass eine Zeichenkette so oft ausgibt, wie die Zeichenkette lang ist.
Bei jeder ausgabe soll die Zeichenkette vorne um 1 Zeichen abgeschnitten werden und dieses Zeichen soll ans Ende gehängt werden.
Beispiel:

Hallo, Welt!
allo, Welt! H
llo, Welt! Ha
lo, Welt! Hal
o, Welt! Hall
, Welt! Hallo
 Welt! Hallo,
Welt! Hallo,
elt! Hallo, W
lt! Hallo, We
t! Hallo, Wel
! Hallo, Welt
 Hallo, Welt!
 