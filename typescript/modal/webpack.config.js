const webpack = require('webpack');
const path = require('path');

module.exports = {
    entry: './src/index.ts',
    output: {
        filename: 'index.js',
        path: path.resolve(__dirname, 'static/js/'),
        publicPath: 'static/js'
    },
    devtool: 'source-map',
    devServer: {
        contentBase: __dirname,
        hot: true,
        publicPath: '/static/js/',
        port: 8001,
        watchContentBase: true
    },
    module: {
        rules: [
            {
                test: /\.css$/, use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.ts$/,
                'exclude': /node_modules/,
                use: 'ts-loader'
            }
        ]
    },
    performance: {
        maxEntrypointSize: 400000,
        maxAssetSize: 400000
    },
    resolve: {
        extensions: ['.ts', '.js', '.json']
    }
};
