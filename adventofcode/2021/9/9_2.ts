const INPUT_FILE = "./input9.txt";
// const INPUT_FILE = "./input9_test.txt";

const text: string = await Deno.readTextFile(INPUT_FILE);
const heightmap: number[][] =
    text.split("\n")
        .map(
            line => line.split("")
                .map(
                    digit => parseInt(digit, 10)
                )
        );

function getNeighbors(x: number, y: number): number[][] {
    const neighbors: number[][] = [];
    if (x > 0) {
        neighbors.push([x - 1, y]);
    }
    if (x < heightmap.length - 1) {
        neighbors.push([x + 1, y]);
    }
    if (y > 0) {
        neighbors.push([x, y - 1]);
    }
    if (y < heightmap[x].length - 1) {
        neighbors.push([x, y + 1]);
    }
    return neighbors;
}

const low_points: { coordinates: number[], size: number }[] = [];

for (let i = 0; i <= heightmap.length - 1; i++) {
    for (let j = 0; j <= heightmap[i].length - 1; j++) {
        const neighbors = getNeighbors(i, j);
        const min = Math.min(...neighbors.map(([x, y]) => heightmap[x][y]));
        if (heightmap[i][j] < min) {
            const low_point = {
                coordinates: [i, j],
                size: 0,
            };
            low_points.push(low_point);
        }
    }
}

function calculateBasinSize(x: number, y: number): number {
    const neighbors = getNeighbors(x, y);
    heightmap[x][y] = 9;
    let sum = 0;
    for (const [x, y] of neighbors) {
        if (heightmap[x][y] < 9) {
            sum += calculateBasinSize(x, y);
        }
    }
    return 1 + sum;
}

function printHeightmap() {
    for (const line of heightmap) {
        console.log(line.join(""));
    }
}

for (let low_point of low_points) {
    low_point.size = calculateBasinSize(low_point.coordinates[0], low_point.coordinates[1]);
}

low_points.sort((a, b) => b.size - a.size);

let product = 1;

for (let i = 0; i < 3; i++) {
    product *= low_points[i].size;
}

console.log(low_points);
console.log(product);