#!/usr/bin/env python3
#-*- coding:utf-8 -*-
import os
import fontname

fonts = "/home/mogoh/.local/share/fonts/"

#Check if only .ttf and .otf files are available
#todo

#Rename all files

#todo upper to lower case
#todo remove whitespaces
for file in os.listdir(fonts):
    if file[-4:] == ".ttf" or file[-4:] != ".otf":
        extension = file[-4:]
        newName = fontname.shortName(fonts+file)[0].strip()+extension
        os.rename(fonts+file, fonts+newName)
        print(newName)

#Remove doublicated files
for file in os.listdir(fonts):
    if file[-4:] == ".ttf":
        for file2 in os.listdir(fonts):
            if file2[-4:] == ".otf":
                if file[:-4] == file2[:-4]:
                    print(file)
                    print(file2)
                    os.remove(fonts+file)


