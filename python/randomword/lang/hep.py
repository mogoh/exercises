#!/usr/bin/env python3
#-*- coding:utf-8 -*- 

import random

#Plosive
plosive = [
["p",1000], #p Stimmloser bilabialer Plosiv (deutsches p)
["b",1000], #b Stimmhafter bilabialer Plosiv (deutsches b)
["t",1000], #t Stimmloser alveolarer Plosiv (deutsches t)
["d",1000], #d Stimmhafter alveolarer Plosiv (deutsches d)
["k",1000], #k Stimmloser velarer Plosiv (deutsches k kamm, sack, christ, fuchs)
["g",1000] #g Stimmhafter velarer Plosiv (deutsches g)
]

# Frikative
fricative = [
["f",1000], #f Stimmloser labiodentaler Frikativ (deutsches f, ff, v, ph)
["w",1000], #v Stimmhafter labiodentaler Frikativ (deutsch: wunsch)
["þ",1000], #θ Stimmloser dentaler Frikativ (englisch: thanks)
["ss",1000], #s Stimmloser alveolarer Frikativ (deutsch: ss, ß Eis)
["s",1000], #z Stimmhafter alveolarer Frikativ (deutsch: Sage, Hase)
["sh",1000], #ʃ Stimmloser postalveolarer Frikativ (deutsch: Fisch, Stimme, Spät)
["ch",1000], #ç Stimmloser palataler Frikativ (deutsch: ich möchte reiche mädchen)
["χ",1000], #χ Stimmloser uvularer Frikativ
["h",1000] #h Stimmloser glottaler Frikativ (deutsches h)
]

#Nasale
nasal = [
["m",1000], #m Stimmhafter bilabialer Nasal (deutsches m)
["n",1000] #n Stimmhafter alveolarer Nasal (deutsches n)
]

#Vibranten
trill = [
["r",1000] #ʀ Stimmhafter uvularer Vibrant (realisation des r)
]

#Approximanten
approximant = [
["j",1000] #j Stimmhafter palataler Approximant (deutsches j)
]

onsetcc = [
['pss', 100],
['ssr', 100],
['th', 100],
['fj', 100],
['dʒ', 100],
['sht', 100],
['ps', 100],
['ph', 100],
['pf', 100],
['tsh', 100],
['pch', 100],
['gh', 100],
['', 5000]
]

codacc = [
['χk', 100],
['kss', 100],
['nk', 100],
['ssb', 100],
['pch', 100],
['bsh', 100],
['ssd', 100],
['mt', 100],
['mʒ', 100],
['sk', 100],
['ng', 100],
['', 10000]
]

vocals = [
["ie",100], #i Ungerundeter geschlossener Vorderzungenvokal i, ie, ih
["i",1000], #ɪ Ungerundeter zentralisierter fast geschlossener Vorderzungenvokal i (Kurzes i: mitte)
["e",1000], #e Ungerundeter halbgeschlossener Vorderzungenvokal e (beten, xenographie)
["ä",10], #ɛ Ungerundeter halboffener Vorderzungenvokal e (nett, echt)
["a",1000], #a Ungerundeter offener Zentralvokal (Kein IPA-Zeichen!) (Tal, Blatt, daheim, Banane)
["oo",100], #o Gerundeter halbgeschlossener Hinterzungenvokal (Bote)
["o",1000], #ɔ Gerundeter halboffener Hinterzungenvokal o (Boxen, offen, Tonne)
["uu",100], #u Gerundeter geschlossener Hinterzungenvokal (gut)
["u",1000], #ʊ Gerundeter zentralisierter fast geschlossener Hinterzungenvokal (Kurzes u: Butter, Urteil)
["üü",1], #y Gerundeter geschlossener Vorderzungenvokal ü, üh, y
["ü",10], #ʏ Gerundeter zentralisierter fast geschlossener Vorderzungenvokal ü (kurzes ü: Hündin, Mücke)
["öö",1], #ø Gerundeter halbgeschlossener Vorderzungenvokal ö, öh (Öl, Höhle)
["ö",10] #œ Gerundeter halboffener Vorderzungenvokal kurzes ö (röcheln)
]
"""
random.seed(a="a")
for a in vocals:
    a[1] = a[1]*random.randint(1,1000)
random.seed()
"""