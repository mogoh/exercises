"use strict";
async function establishSignalConnection() {
    return new Promise((resolve, reject) => {
        const url = `ws://${window.location.host}`;
        const signal = new WebSocket(url);
        signal.onopen = () => {
            console.debug('WebSocket signal connection established', url);
            resolve(signal);
        };
    });
}
establishSignalConnection();
