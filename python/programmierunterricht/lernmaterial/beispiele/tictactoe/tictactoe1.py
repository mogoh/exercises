# Tic Tac Toe

import random

def zeichneSpielbrett(spielbrett):
    '''Zeichne das Spielspielbrett.
    '''
    print(f'{spielbrett[7]}|{spielbrett[8]}|{spielbrett[9]}    7|8|9')
    print('-+-+-    -+-+-')
    print(f'{spielbrett[4]}|{spielbrett[5]}|{spielbrett[6]}    4|5|6')
    print('-+-+-    -+-+-')
    print(f'{spielbrett[1]}|{spielbrett[2]}|{spielbrett[3]}    1|2|3')
    
    
# Leere Spielbrett
spielbrett = [' '] * 10
zeichneSpielbrett(spielbrett)
