# Einzeilige Kommentare beginnen mit einer Raute (Doppelkreuz)
# Der Computer igoniert alles, was mit einer Raute beginnt.

# Zeichenketten (im englischen "Strings") ist Text, der mit einfachen oder doppelten Anführungszeichen umschlossen ist.
# Zeichenketten 

# Die Print-Funktion gibt einfach einen Text aus. Zum Beispiel so:

print("Hallo, Welt!")

# Soll eine Zeichenkette über mehrere Zeilen gehen, müssen drei Anführungszeichen verwendet werden:

'''
Zum Beispiel so.
'''

"""
Oder
so.
"""

# Der Computer ignoriert Zeichenketten, die nicht weiter verwendet werden.
# Deswegen kann man diese auch zum kommentieren benutzen.

""" Mehrzeilige Zeichenketten werden mit
    drei '-Zeichen geschrieben und werden
    oft als Kommentare genutzt.
"""

print('Hallo, Welt2!')

print(
'''Hallo
kann
man
auch
über
mehrere
Zeilen
schreiben.''')