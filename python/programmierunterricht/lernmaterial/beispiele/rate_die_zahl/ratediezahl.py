# Dies ist das »Rate-die-Zahl«-Spiel.
from random import randint

versucheUnternommen = 0

print('Hallo! Wie ist dein Name?')
meinName = input()

# Zufallszahl
zahl = randint(1, 20)

print('Nun, ' + meinName + ', ich denke mir gerade eine Zahl zwischen 1 und 20 aus.')

while versucheUnternommen <= 5:
    print('Rate bitte jetzt eine Zahl:')
    versuch = input()
    versuch = int(versuch)

    if versuch < zahl:
        print('Deine Zahl ist zu klein.')

    if versuch > zahl:
        print('Deine Zahl ist zu groß.')

    if versuch == zahl:
        break

    print(versucheUnternommen)
    versucheUnternommen = versucheUnternommen + 1
    print(versucheUnternommen)

if versuch == zahl:
    versucheUnternommen = str(versucheUnternommen + 1)
    print('Sehr gut, ' + meinName + '! Du hast die Zahl nach ' +
        versucheUnternommen + ' Versuchen erraten!')

if versuch != zahl:
    zahl = str(zahl)
    print('Nein, die Zahl, an die ich gedacht habe, war:  ' + zahl)