const INPUT_FILE = "./input5.txt";

const text: string = await Deno.readTextFile(INPUT_FILE);

let lines: number[][] =
    text.split("\n")
        .map(
            line => line.split(" -> ")
                .map(
                    pair => pair.split(",")
                        .map(number => parseInt(number, 10))
                ).flat()
        );

// Order direction
lines = lines.map(line => {
    if (line[0] > line[2] || line[1] > line[3]) {
        return [line[2], line[3], line[0], line[1]];
    } else {
        return line;
    }
})

// Create area
let area: number[][] = new Array(1000).fill(0).map(() => new Array(1000).fill(0));

for (let line of lines) {
    // top to bottom
    if (line[0] === line[2]) {
        let x: number = line[0];
        let y: number = line[1];
        while (y <= line[3]) {
            area[x][y] = area[x][y] + 1;
            y += 1;
        }
    } else
    // left to right
    if (line[1] === line[3]) {
        let x: number = line[0];
        let y: number = line[1];
        while (x <= line[2]) {
            area[x][y] = area[x][y] + 1;
            x += 1;
        }
    } else
    // top left to bottom right
    if (line[0] <= line[2] && line[1] <= line[3]) {
        let x: number = line[0];
        let y: number = line[1];
        while (x <= line[2]) {
            area[x][y] = area[x][y] + 1;
            x += 1;
            y += 1;
        }
    } else
    // top right to bottom left
    if (line[0] >= line[2] && line[1] <= line[3]) {
        let x: number = line[0];
        let y: number = line[1];
        while (x >= line[2]) {
            area[x][y] = area[x][y] + 1;
            x -= 1;
            y += 1;
        }
    } else 
    // bottom left to top right
    if (line[0] <= line[2] && line[1] >= line[3]) {
        let x: number = line[0];
        let y: number = line[1];
        while (x <= line[2]) {
            area[x][y] = area[x][y] + 1;
            x += 1;
            y -= 1;
        }
    }
}

console.log(area.reduce((acc, row) => acc + row.reduce((acc, cell) => acc + (cell >= 2 ? 1 : 0), 0), 0))
