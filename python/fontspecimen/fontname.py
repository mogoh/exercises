#!/usr/bin/env python3
#-*- coding:utf-8 -*-
import sys
from fontTools import ttLib

def shortName( fontFile ):
    """Get the short name from the font's names table"""
    FONT_SPECIFIER_NAME_ID = 4
    FONT_SPECIFIER_FAMILY_ID = 1
    font = ttLib.TTFont(fontFile)
    name = ""
    family = ""
    for record in font['name'].names:
        if record.nameID == FONT_SPECIFIER_NAME_ID and not name:
            if b'\000' in record.string:
                name = str(record.string, 'utf-16-be')
            else:
                name = str(record.string, "utf-8")
        elif record.nameID == FONT_SPECIFIER_FAMILY_ID and not family:
            if b'\000' in record.string:
                family = str(record.string, 'utf-16-be')
            else:
                family = str(record.string, "utf-8")
        if name and family:
            break
    return name, family

if __name__ == "__main__":
    arg1 = sys.argv[1]
    short = shortName(arg1)
    print(short[0])
