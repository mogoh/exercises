# Tic Tac Toe

import random

def zeichneSpielbrett(spielbrett):
    '''Zeichne das Spielspielbrett.
    '''
    print(f'{spielbrett[7]}|{spielbrett[8]}|{spielbrett[9]}    7|8|9')
    print('-+-+-    -+-+-')
    print(f'{spielbrett[4]}|{spielbrett[5]}|{spielbrett[6]}    4|5|6')
    print('-+-+-    -+-+-')
    print(f'{spielbrett[1]}|{spielbrett[2]}|{spielbrett[3]}    1|2|3')

def eingabeSpielerBuchstabe():
    '''Erhalte Eingabe des Buchstabens des Spielers.
    '''
    buchstabe = ''
    while not (buchstabe == 'X' or buchstabe == 'O'):
        print('Möchtest du »X« oder »O« sein?')
        buchstabe = input().upper()

    # Das Erste Element der List ist der Buchstabe des Spielers, das Zweite Element ist der Buchstabe des Computers.
    if buchstabe == 'X':
        return ['X', 'O']
    else:
        return ['O', 'X']

def werIstErster():
    '''Wähle zufällig aus, welcher der beiden Spieler zuerst spielt.
    '''
    if random.randint(0, 1) == 0:
        return 'computer'
    else:
        return 'spieler'

def macheZug(spielbrett, buchstabe, zug):
    spielbrett[zug] = buchstabe

def istGewinner(spielbrett, buchstabe):
    ''' Test, für ein Spielbrett, ob ein Buchstabe (Spieler) gewonnen hat.

    Die Funktion nimmt ein Brett und ein Buchstabe und teste ob dieser gewonnen hat.
    '''
    return (
        (spielbrett[7] == buchstabe and spielbrett[8] == buchstabe and spielbrett[9] == buchstabe) or # Reihe oben
        (spielbrett[4] == buchstabe and spielbrett[5] == buchstabe and spielbrett[6] == buchstabe) or # Reihe mitte
        (spielbrett[1] == buchstabe and spielbrett[2] == buchstabe and spielbrett[3] == buchstabe) or # Reihe unten
        (spielbrett[7] == buchstabe and spielbrett[4] == buchstabe and spielbrett[1] == buchstabe) or # Spalte links
        (spielbrett[8] == buchstabe and spielbrett[5] == buchstabe and spielbrett[2] == buchstabe) or # Spalte mitte
        (spielbrett[9] == buchstabe and spielbrett[6] == buchstabe and spielbrett[3] == buchstabe) or # Spalte rechts
        (spielbrett[7] == buchstabe and spielbrett[5] == buchstabe and spielbrett[3] == buchstabe) or # Diagonal
        (spielbrett[9] == buchstabe and spielbrett[5] == buchstabe and spielbrett[1] == buchstabe) # Diagonal
    )

def kopiereSpielbrett(spielbrett):
    '''Erstelle eine Kopie des Spielspielbretts.
    '''
    spielbrettKopie = []
    for i in spielbrett:
        spielbrettKopie.append(i)
    return spielbrettKopie

def istFeldFrei(spielbrett, zug):
    '''Test, ob ein Feld frei ist.

    Diese Funktion testet, ob ein Feld frei ist oder ob es bereits belegt wurde.
    '''
    return spielbrett[zug] == ' '

def erhalteSpielerzug(spielbrett):
    '''Las den Spieler einen Zug eingeben.
    '''
    zug = ' '
    möglicherZug = ['1','2','3','4','5','6','7','8','9']
    while zug not in möglicherZug or not istFeldFrei(spielbrett, int(zug)):
        print('Was ist dein nächster Zug? (1-9)')
        zug = input()
    return int(zug)

def wähleZufälligenZugAusListe(spielbrett, zügeListe):
    '''Wähle zufällig einen Zug aus einer Liste aus.

    Diese Funktion wählt aus einer Liste möglicher Züge einen zufälligen Zug aus. Wenn der Zug ungülig ist gibt die Funktion 'None' zurück.
    '''
    möglicherZug = []
    for i in zügeListe:
        if istFeldFrei(spielbrett, i):
            möglicherZug.append(i)

    if len(möglicherZug) != 0:
        return random.choice(möglicherZug)
    else:
        return None

def erhalteComputerZug(spielbrett, computerBuchstabe):
    '''Mache einen Zug.

    Diese Funktion erhält ein Spielbrett und den Buchstaben des Computers und macht einen Zug.
    '''
    if computerBuchstabe == 'X':
        spielerBuchstabe = 'O'
    else:
        spielerBuchstabe = 'X'

    # Hier ist der Algorithmus für unsere Künstliche Intelligenz
    # Prüfe, ob der Computer im nächste Zug gewinnen kann.
    for i in range(1, 10):
        spielbrettKopie = kopiereSpielbrett(spielbrett)
        if istFeldFrei(spielbrettKopie, i):
            macheZug(spielbrettKopie, computerBuchstabe, i)
            if istGewinner(spielbrettKopie, computerBuchstabe):
                return i

    # Prüfe, ob der Spieler im nächste Zug gewinnen kann.
    for i in range(1, 10):
        spielbrettKopie = kopiereSpielbrett(spielbrett)
        if istFeldFrei(spielbrettKopie, i):
            macheZug(spielbrettKopie, spielerBuchstabe, i)
            if istGewinner(spielbrettKopie, spielerBuchstabe):
                return i

    # Nimm eine der Ecken, wenn sie noch Frei sind.
    zug = wähleZufälligenZugAusListe(spielbrett, [1, 3, 7, 9])
    if zug != None:
        return zug

    # Nimm die Mitte, wenn sie noch Frei ist.
    if istFeldFrei(spielbrett, 5):
        return 5

    # Nimm einen der Ränder.
    return wähleZufälligenZugAusListe(spielbrett, [2, 4, 6, 8])

def istBrettVoll(spielbrett):
    '''Gibt 'True' zurück, wenn jedes Feld vom Brett belegt ist, 'False' sonst.
    '''
    for i in range(1, 10):
        if istFeldFrei(spielbrett, i):
            return False
    return True


print('Willkommen im Spiel Tic Tac Toe!')

while True:
    # Leere Spielbrett
    spielbrett = [' '] * 10
    spielerBuchstabe, computerBuchstabe = eingabeSpielerBuchstabe()
    anDerReihe = werIstErster()
    print(f'Spieler {anDerReihe} ist zuerst an der Reihe.')
    spielLäuft = True

    while spielLäuft:
        if anDerReihe == 'spieler':
            # Spieler/Spielerin ist an der Reihe
            zeichneSpielbrett(spielbrett)
            zug = erhalteSpielerzug(spielbrett)
            macheZug(spielbrett, spielerBuchstabe, zug)

            if istGewinner(spielbrett, spielerBuchstabe):
                zeichneSpielbrett(spielbrett)
                print('Juchhu! Spieler, du hast gewonnen!')
                spielLäuft = False
            else:
                if istBrettVoll(spielbrett):
                    zeichneSpielbrett(spielbrett)
                    print('Unentschieden!')
                    break
                else:
                    anDerReihe = 'computer'

        else:
            # Spieler/Spielerin ist an der Reihe
            zug = erhalteComputerZug(spielbrett, computerBuchstabe)
            macheZug(spielbrett, computerBuchstabe, zug)

            if istGewinner(spielbrett, computerBuchstabe):
                zeichneSpielbrett(spielbrett)
                print('Der Computer hat gewonnen.')
                spielLäuft = False
            else:
                if istBrettVoll(spielbrett):
                    zeichneSpielbrett(spielbrett)
                    print('Unentschieden!')
                    break
                else:
                    anDerReihe = 'spieler'

    print('Möchtest du noch einmal spielen? (ja oder nein)')
    if not input().lower().startswith('j'):
        break

