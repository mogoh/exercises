// Tiny Sprite Sheet Generator - Frank Force 2020 - MIT License
import { PRNG } from './PRNG.js';


const canvas = document.querySelector('canvas') as HTMLCanvasElement;
const context = canvas.getContext('2d') as CanvasRenderingContext2D;
// set 2 pixel wide line width to make the black outline
context.lineWidth = 2;
// seed for random generaton, can be replaced with hardcoded value
const seed = PRNG.randomSeed();
// You could pick your favourit number
// const seed = 2;
const prng = new PRNG(seed);
const sprite_size = 16;
const rows = 1;
const columns = 1;


for (let row = 0; row < rows; row += 1) {
    const top = row * sprite_size;
    for (let column = 0; column < columns; column += 1) {
        const left = column * sprite_size;
        // 4 passes, outline left/right and fill left/right
        // randomize color
        context.fillStyle = `rgb(${prng.nextInt(0, 255)},${prng.nextInt(0, 255)},${prng.nextInt(0, 255)})`;
        context.fillRect(left, top, sprite_size, sprite_size);
        const seed = prng.nextInt(0, 2 ** 32);
        // for (let i = sprite_size; i >= 0; i -= 1) {
        for (let pass = 0; pass <= 3; pass += 1) {
            const prng = new PRNG(seed);
            for (let j = prng.nextInt(50, 110); j >= 0; j -= 1) {
                // x & y pixel index in sprite
                // max 7, descending
                const x = j % 8;
                // max 12, descending in chunks
                const y = Math.floor(j / 8);
                // x pos, flipped if pass is even
                const x_absolute = left + 7 - (pass % 2) * 2 * x + x;
                const y_absolute = top + 2 + y;
                // small chance of new color
                if (prng.nextNumber() < 0.07) {
                    // randomize color
                    context.fillStyle = `rgb(${prng.nextInt(0, 255)},${prng.nextInt(0, 255)},${prng.nextInt(0, 255)})`;
                }
                if (prng.nextNumber() < 0.9) {
                    // distance from center vs random number
                    if (prng.nextInt(0, 32) > x ** 2 + (y - 5) ** 2) {
                        if (pass <= 1) {
                            context.strokeRect(x_absolute, y_absolute, 1, 1);
                        } else {
                            context.fillRect(x_absolute, y_absolute, 1, 1);
                        }
                    }
                }
            }
        }
    }
}
