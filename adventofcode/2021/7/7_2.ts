const INPUT_FILE = "./input7.txt";
// const INPUT_FILE = "./input7_test.txt";

const text: string = await Deno.readTextFile(INPUT_FILE);
const positions: number[] = text.split(",").map(Number);

function calculate_fuel_consumption(positions: number[], target: number): number {
    let fuel = 0;
    positions.forEach(p => {
        let delta = Math.abs(p - target);
        fuel += (delta * (delta+1))/2;
    });
    return fuel;
}

positions.sort((a, b) => a - b);

const smallest = positions[0];
const largest = positions[positions.length - 1];

let target = smallest;
let target_fuel = calculate_fuel_consumption(positions, target);

for (let i = smallest; i <= largest; i++) {
    const fuel = calculate_fuel_consumption(positions, i);
    if (fuel < target_fuel) {
        target = i;
        target_fuel = fuel;
    }
    // console.log(`${i} => ${fuel}`);
}

const fuel_consumption = calculate_fuel_consumption(positions, target);

console.log(`${target} => ${target_fuel}`);