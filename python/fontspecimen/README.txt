

requires:
python 3.5
FontTools python package
ImageMagick
poppler utils pdfunite
firefox
extension cmdlprint https://github.com/eclipxe13/cmdlnprint
about:config Set 
security.fileuri.strict_origin_policy to false

rename font files (optional)
remove double font files
remove spaces from font files
./rename.py

./fontspecimen.py

./convert.py

pdfunite pdf/*.pdf fonts.pdf


ToDo:
  * Fix Readme
  * Console Parameter
    * language
    * path
    * help text
  * check for other font formats
  * test for windows
  * only print available characters
