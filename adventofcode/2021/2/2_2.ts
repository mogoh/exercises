const INPUT = "./input2.txt";

const text = await Deno.readTextFile(INPUT);

const course: [string, number][] = text.split("\n").map(
    line => {
        let direction: string, distance: string;
        [direction, distance] = line.split(" ");
        return [direction, parseInt(distance, 10)];
    }
);

let depth = 0;
let forward = 0;
let aim = 0;

course.forEach(command => {
    const [direction, distance] = command;
    if (direction === "down") {
        aim += distance;
    } else if (direction === "up") {
        aim -= distance;
    } else if (direction === "forward") {
        forward += distance;
        depth += aim * distance;
    }
    console.log(`aim: ${aim}, depth: ${depth}, forward: ${forward}`);
});

let scalar_product: number = forward * depth;

console.log(scalar_product);
