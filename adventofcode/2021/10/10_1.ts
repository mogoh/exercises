const INPUT_FILE = "./input10.txt";
// const INPUT_FILE = "./input10_test.txt";
// const INPUT_FILE = "./input10_test2.txt";
const TEXT: string = await Deno.readTextFile(INPUT_FILE);

class Task10 {
    lines: string[] = [];
    score: number = 0;

    constructor() {
        this.parse();
    }

    parse() {
        this.lines = TEXT.split("\n");
    }

    increaseScore(top: string) {
        switch (top) {
            case ")":
                this.score += 3;
                break;
            case "]":
                this.score += 57;
                break;
            case "}":
                this.score += 1197;
                break;
            case ">":
                this.score += 25137;
                break;
        }
    }

    run() {
        console.log("Task 10");

        for (let line of this.lines) {
            let stack: string[] = [];
            for(let i = 0; i < line.length; i++) {
                switch (line[i]) {
                    case "(":
                    case "[":
                    case "{":
                    case "<":
                        stack.push(line[i]);
                        break;
                    case ")":
                        if (stack.length === 0) {
                            console.log("Error: Missing opening bracket:", line[i]);
                        } else {
                            let top = stack.pop()!;
                            if (top != "(") {
                                this.increaseScore(")");
                                console.log("Error: Expected ')', got '" + top + "'");
                            }
                        }
                        break;
                    case "]":
                        if (stack.length === 0) {
                            console.log("Error: Missing opening bracket:", line[i]);
                        } else {
                            let top = stack.pop()!;
                            if (top != "[") {
                                this.increaseScore("]");
                                console.log("Error: Expected ']', got '" + top + "'");
                            }
                        }
                        break;
                    case "}":
                        if (stack.length === 0) {
                            console.log("Error: Missing opening bracket:", line[i]);
                        } else {
                            let top = stack.pop()!;
                            if (top != "{") {
                                this.increaseScore("}");
                                console.log("Error: Expected '}', got '" + top + "'");
                            }
                        }
                        break;
                    case ">":
                        if (stack.length === 0) {
                            console.log("Error: Missing opening bracket:", line[i]);
                        } else {
                            let top = stack.pop()!;
                            if (top != "<") {
                                this.increaseScore(">");
                                console.log("Error: Expected '>', got '" + top + "'");
                            }
                        }
                        break;
                }
            }
            if (stack.length > 0) {
                console.log("Error: Missing closing bracket(s):", stack);
            }
        }

        console.log("Score:", this.score);
    }
}

const task10 = new Task10();
task10.run();
