import { t, ta, tc, tac } from "./ultrascript.js";

const e =
    [
        tac(
            "h1",
            [["a", "b"]],
            ["Foo"],
        ),
        tc(
            "p",
            ["Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."]
        ),
        t("br"),
        ta("div", [["fdsa", "fwefwe"]]),

    ];

// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
const body = document.querySelector("body")!;
body.innerHTML = "";
//body.append();
e.map(e => e.render()).forEach(e => body.append(e));