# Wir importieren die relevanten Pygame-Module und Variablen:
import sys
import os
from random import randint
import pygame
from pygame.locals import *

# Wo befindet sich unser Python-Skript?
PFAD = os.path.dirname(os.path.realpath(__file__))

# Pygame muss zunächst initialisiert werden:
pygame.init()

# Bilder pro Sekunde
BPS = 60
# Die bpsUhr (Bilder pro Sekunde-Uhr) hilft uns dabei, dass ein Programm nicht 
# schneller läuft, als es sollte. Wir brauchen die bpsUhr später. 30 BPS
# bedeutet alle 1/60 = 0,01666... Sekunden erzeugen wir ein neues Bild.
bpsUhr = pygame.time.Clock()

# set up the window
BREITE = 800
HÖHE = 800
ANZEIGEFLÄCHE = pygame.display.set_mode((BREITE, HÖHE), 0, 32)
pygame.display.set_caption('Bananenjagd')

# Farbe weiß definiert als ein 3-Tupel
#        rot grün blau
WEISS = (255, 255, 255)
ROT =   (255,   0,   0)

# Richtungen
HOCH = 'hoch'
RUNTER = 'runter'
LINKS = 'links'
RECHTS = 'rechts'
HALT = 'halt'

# Wir laden ein Hintergrundbild.
hintergrundbild_pfad = os.path.join(PFAD, 'hintergrund.jpg')
hintergrundbild = pygame.image.load(hintergrundbild_pfad)

# Wir laden das Bild einer Banane:
bananebild_pfad = os.path.join(PFAD, 'banane.png')
bananebild = pygame.image.load(bananebild_pfad)
# Größe:
banane_breite = 17
banane_höhe = 15
# Startposition der Banane
banane_x = randint(10, BREITE-banane_breite-11)
banane_y = randint(10, HÖHE-banane_höhe-11)

# Wir laden das Bild einer Katze
katzenbild_pfad = os.path.join(PFAD, 'katze.png')
katzenbild = pygame.image.load(katzenbild_pfad)
# Größe:
katze_breite = 115
katze_höhe = 75
# Startposition der Katze
katze_x = randint(0, BREITE-katze_breite-1) 
katze_y = randint(0, HÖHE-katze_höhe-1) 

# Richtung gibt an, in welche Richtung sich die Katze initial bewegen soll.
richtung = HALT

# Wie schnell bewegt sich die Katze?
geschwindigkeit = 1

# Haupt-Spiel-Schleife
while True:
    # Wir überprüfen die Eingabe:
    for ereignis in pygame.event.get():
        # Wenn der Nutzer das Programm beenden möchte:
        if ereignis.type == QUIT:
            pygame.quit()
            sys.exit()
        # Wir Prüfen, ob eine Taste gedrückt wurde:
        if ereignis.type == KEYDOWN:
            if ereignis.key == K_LEFT:
                richtung = LINKS
            elif ereignis.key == K_RIGHT:
                richtung = RECHTS
            elif ereignis.key == K_UP:
                richtung = HOCH
            elif ereignis.key == K_DOWN:
                richtung = RUNTER
            elif ereignis.key == K_ESCAPE:
                pygame.quit()
                sys.exit()

    # Wir bewegen die Katze:
    if richtung == RECHTS:
        katze_x += geschwindigkeit
    if richtung == RUNTER:
        katze_y += geschwindigkeit
    if richtung == LINKS:
        katze_x -= geschwindigkeit
    if richtung == HOCH:
        katze_y -= geschwindigkeit
    
    # Wir Prüfen auf Kollision:
    # Wenn die Katze die Banane überdeckt:
    if (    banane_x >= katze_x and 
            banane_x+banane_breite <= katze_x+katze_breite and 
            banane_y >= katze_y and 
            banane_y+banane_höhe <= katze_y+katze_höhe):
        # Erhöhe die Geschwindigkeit:
        geschwindigkeit += 1
        # Bewege die Banane zu einer neuen Position:
        banane_x = randint(10, BREITE-banane_breite-11)
        banane_y = randint(10, HÖHE-banane_höhe-11)
    if (    katze_x < 0 or 
            katze_x+katze_breite > BREITE or
            katze_y < 0 or
            katze_y+katze_höhe > HÖHE):
        print("Ende")
        print("Punkte: "+str(geschwindigkeit-1))
        pygame.quit()
        sys.exit()


    # Wir füllen die Anzeigefläche mit weißer Farbe:
    ANZEIGEFLÄCHE.fill(WEISS)

    # blit: Bit Block Image Transfer - Bewege ein Bild (auf eine für den
    # Computer einfache Weise).
    # Zeichne den Hintergrund. 
    ANZEIGEFLÄCHE.blit(hintergrundbild, (0, 0))
    # Zeichne die Banane:
    ANZEIGEFLÄCHE.blit(bananebild, (banane_x, banane_y))
    # Zeichne die Kazte:
    ANZEIGEFLÄCHE.blit(katzenbild, (katze_x, katze_y))

    # Wir haben die Anzeigefläche bearbeitet. Nun muss der Bildschirm 
    # akualisiert werden.
    pygame.display.update()
    # Jetzt müssen wir warten, bis genügend Zeit vergangen ist.
    bpsUhr.tick(BPS)

def zeichne_gitter():
    gittergröße = 80
    gitterfarbe = ROT
    spalte = 10
    reihen = 10
    for x in range(spalte + 1):
        