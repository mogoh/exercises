def zeichneSpielfeld(spielfeld):
    print(f'{spielfeld[1]}|{spielfeld[2]}|{spielfeld[3]}')
    print('-+-+-')
    print(f'{spielfeld[4]}|{spielfeld[5]}|{spielfeld[6]}')
    print('-+-+-')
    print(f'{spielfeld[7]}|{spielfeld[8]}|{spielfeld[9]}')

spielfeld = [' ',' ',' ',' ',' ',' ',' ',' ',' ',' ']
zeichneSpielfeld(spielfeld)


def erhalteSpielerzug():
    position = int(input())
    spielfeld[position] = "x"
    zeichneSpielfeld(spielfeld)

erhalteSpielerzug()
