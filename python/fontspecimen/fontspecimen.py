#!/usr/bin/env python3
#-*- coding:utf-8 -*-
import os
import fontname
from time import sleep
from string import Template
from subprocess import run
import glob
import ntpath

workingdir = os.path.dirname(os.path.realpath(__file__))
homedir = os.path.expanduser("~")
language = "de"
fonts = homedir+"/.local/share/fonts"
#fonts = homedir+"/.fonts"
#fonts = "/usr/share/fonts"
#fonts = "/usr/local/share/fonts"
#fonts = homedir+"/temp"

with open ("fontspecimen."+language+".html", "r") as myfile:
    template = Template(myfile.read())

def createSpecimen(fontfile):
    path, file = ntpath.split(fontfile)
    htmlfile = "html/"+file[:-3]+"html"
    pdffile = "pdf/"+file[:-3]+"pdf"
    pngfile = "png/"+file[:-3]+"png"
    fname = fontname.shortName(fontfile)[0]
    html = template.substitute(fontname=fname, fontfile=fontfile)
    
    with open(htmlfile, "w") as myfile:
        myfile.write(html)

    MARGIN = "0"
    cmd = [
    "firefox",
    "-print-shrinktofit", "no",
#    "-print-paper-custom", "yes",
#    "-print-paper-units", "mm",
#    "-print-paper-width", "210",
#    "-print-paper-height", "297",
    "-print-header", "no",
    "-print-footer", "no",
    "-print-range-start", "1", #does not work?
    "-print-range-end", "1", #does not work?
    "-print", workingdir+"/"+htmlfile,
    "-print-mode", "pdf",
    "-print-file", workingdir+"/"+pdffile,
    "-print-margin-top", MARGIN,
    "-print-margin-right", MARGIN,
    "-print-margin-bottom", MARGIN,
    "-print-margin-left", MARGIN,
    ]
    run(" ".join(cmd), stdout=open(os.devnull, 'w'), stderr=open(os.devnull, 'w'), shell=True)
    sleep(2)

for fontfile in sorted(glob.iglob(fonts+"/**", recursive=True)):
    if os.path.isfile(fontfile):
        if fontfile[-3:] == "ttf" or fontfile[-3:] == "otf":
            createSpecimen(fontfile)
