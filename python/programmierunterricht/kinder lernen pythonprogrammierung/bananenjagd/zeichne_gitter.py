def zeichne_gitter():
    gittergröße = 80
    gitterfarbe = ROT
    spalten = 10
    reihen = 10
    for x in range(spalten + 1):
        pygame.draw.line(
            ANZEIGEFLÄCHE,
            gitterfarbe,
            (x * gittergröße - 1, 0), # Startpunkt
            (x * gittergröße - 1, HÖHE), # Endpunkt
            2, # Liniendicke
        )
    for y in range(reihen + 1):
        pygame.draw.line(
            ANZEIGEFLÄCHE,
            gitterfarbe,
            (0, y * gittergröße - 1), # Startpunkt
            (BREITE, y * gittergröße - 1), # Endpunkt
            2, # Liniendicke
        )
