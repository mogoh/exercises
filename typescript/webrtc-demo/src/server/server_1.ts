import * as Koa from "koa";
import * as serve from "koa-static";
import * as websockify from "koa-websocket";
import * as cors from "@koa/cors";

const WEB_PORT = 8000;

const app = websockify(new Koa());

app.use(cors({
    origin: '*'
}));

app.use(serve('./public'));

app.ws.use(async (ctx, next) => {
    return next();
});

app.listen(WEB_PORT, () => {
    console.info(`Server listening on port ${WEB_PORT}`);
});
