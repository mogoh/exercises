const INPUT_FILE = "./input7.txt";

const text: string = await Deno.readTextFile(INPUT_FILE);
const positions: number[] = text.split(",").map(Number);

// Calculating Median
// Not entirely correct, because round up my not correctly calculate the median.
positions.sort((a, b) => a - b);
const i = Math.floor(positions.length/2);
const median = positions[i];

let fuel = 0;
positions.forEach(p => {
    fuel += Math.abs(p - median);
});

console.log(fuel);