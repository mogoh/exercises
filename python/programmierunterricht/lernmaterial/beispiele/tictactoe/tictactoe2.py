# Tic Tac Toe

import random

def zeichneSpielbrett(spielbrett):
    '''Zeichne das Spielspielbrett.
    '''
    print(f'{spielbrett[7]}|{spielbrett[8]}|{spielbrett[9]}    7|8|9')
    print('-+-+-    -+-+-')
    print(f'{spielbrett[4]}|{spielbrett[5]}|{spielbrett[6]}    4|5|6')
    print('-+-+-    -+-+-')
    print(f'{spielbrett[1]}|{spielbrett[2]}|{spielbrett[3]}    1|2|3')

def eingabeSpielerBuchstabe():
    '''Erhalte Eingabe des Buchstabens des Spielers.
    '''
    buchstabe = ''
    while not (buchstabe == 'X' or buchstabe == 'O'):
        print('Möchtest du »X« oder »O« sein?')
        buchstabe = input().upper()

    # Das erste Element der List ist der Buchstabe des Spielers, das zweite Element ist der Buchstabe des Computers.
    if buchstabe == 'X':
        return ['X', 'O']
    else:
        return ['O', 'X']

def erhalteSpielerzug(spielbrett):
    '''Las den Spieler einen Zug eingeben.
    '''
    zug = ' '
    möglicherZug = ['1','2','3','4','5','6','7','8','9',]
    while zug not in möglicherZug:
        print('Was ist dein nächster Zug? (1-9)')
        zug = input()
    return int(zug)

# Leere Spielbrett
spielbrett = [' '] * 10
zeichneSpielbrett(spielbrett)
eingabeSpielerBuchstabe()