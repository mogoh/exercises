# Wir importieren die relevanten Pygame-Module und Variablen:
import sys
import os
from random import randint
import pygame
from pygame.locals import *

# Initialisiere globale Variablen

# Wo befindet sich unser Python-Skript?
PFAD = os.path.dirname(os.path.realpath(__file__))
print(PFAD)

# Pygame muss zunächst initialisiert werden:
pygame.init()

# Bilder pro Sekunde
BPS = 60
# Die bpsUhr (Bilder pro Sekunde-Uhr) hilft uns dabei, dass ein Programm nicht 
# schneller läuft, als es sollte. Wir brauchen die bpsUhr später. 30 BPS
# bedeutet alle 1/60 = 0,01666... Sekunden erzeugen wir ein neues Bild.
bpsUhr = pygame.time.Clock()

# set up the window
BREITE = 800
HÖHE = 800
ANZEIGEFLÄCHE = pygame.display.set_mode((BREITE, HÖHE), 0, 32)
pygame.display.set_caption('Bananenjagd')

# Farbe weiß definiert als ein 3-Tupel
#        rot grün blau
WEISS   = (255, 255, 255)
ROT     = (255,   0,   0)
SCHWARZ = (  0,   0,   0)

# Richtungen
HOCH = 'hoch'
RUNTER = 'runter'
LINKS = 'links'
RECHTS = 'rechts'
HALT = 'halt'

GITTERGRÖSSE = 80
GITTERFARBE = SCHWARZ
SPALTEN = 10
REIHEN = 10

class Objekt:
    bild = None
    breite = None
    höhe = None
    x = None
    y = None
    richtung = None
    nächste_richtung = None
    geschwindigkeit = None


def initialisiere_spiel():
    # Wir laden ein Hintergrundbild.
    bild_pfad = os.path.join(PFAD, 'hintergrund.png')
    hintergrundbild = pygame.image.load(bild_pfad)

    banane = Objekt()
    # Wir laden das Bild einer Banane:
    bild_pfad = os.path.join(PFAD, 'banane2.png')
    banane.bild = pygame.image.load(bild_pfad)
    # Größe:
    banane.breite = 80
    banane.höhe = 80
    # Startposition der Banane
    banane.x = zufällige_x_position()
    banane.y = zufällige_y_position()

    spieler = Objekt()
    # Wir laden das Bild einer spieler
    bild_pfad = os.path.join(PFAD, 'katze2.png')
    spieler.bild = pygame.image.load(bild_pfad)
    # Größe:
    spieler.breite = 80
    spieler.höhe = 80

    # Startposition der spieler
    spieler.x = zufällige_x_position()
    spieler.y = zufällige_y_position()

    # Richtung gibt an, in welche Richtung sich die spieler initial bewegen soll.
    spieler.richtung = HALT
    # Wie schnell bewegt sich die spieler?
    spieler.geschwindigkeit = 1
    return (hintergrundbild, spieler, banane)


def beende_spiel():
    pygame.quit()
    sys.exit()

def prüfe_überdeckung(a, b):
    # Wenn die spieler die Banane überdeckt:
    if (b.x >= a.x and
            b.x + b.breite <= a.x + a.breite and
            b.y >= a.y and
            b.y + b.höhe <= a.y + a.höhe):
        return True
    else:
        return False

def zufällige_x_position():
    # SPALTEN = 10
    # Gittergröße = 80
    
    return randint(0, SPALTEN-1)*GITTERGRÖSSE

def zufällige_y_position():
    return randint(0, REIHEN-1)*GITTERGRÖSSE

def ereigniss(spieler):
    # Wir überprüfen die Eingabe:
    for ereignis in pygame.event.get():
        # Wenn der Nutzer das Programm beenden möchte:
        if ereignis.type == QUIT:
            beende_spiel()
        # Wir Prüfen, ob eine Taste gedrückt wurde:
        if ereignis.type == KEYDOWN:
            if ereignis.key == K_LEFT:
                spieler.nächste_richtung = LINKS
            elif ereignis.key == K_RIGHT:
                spieler.nächste_richtung = RECHTS
            elif ereignis.key == K_UP:
                spieler.nächste_richtung = HOCH
            elif ereignis.key == K_DOWN:
                spieler.nächste_richtung = RUNTER
            elif ereignis.key == K_ESCAPE:
                beende_spiel()

def bewege(spieler, banane):
    # befindet sich der spieler genau auf einem Kästchen?
    if spieler.x % GITTERGRÖSSE == 0 and spieler.y % GITTERGRÖSSE == 0:
        spieler.richtung = spieler.nächste_richtung
    # Wir bewegen die spieler:
    if spieler.richtung == RECHTS:
        spieler.x += spieler.geschwindigkeit
    if spieler.richtung == RUNTER:
        spieler.y += spieler.geschwindigkeit
    if spieler.richtung == LINKS:
        spieler.x -= spieler.geschwindigkeit
    if spieler.richtung == HOCH:
        spieler.y -= spieler.geschwindigkeit

    # Wenn die spieler die Banane überdeckt:
    if prüfe_überdeckung(spieler, banane):
        # Erhöhe die Geschwindigkeit:
        spieler.geschwindigkeit *= 2
        # Bewege die Banane zu einer neuen Position:
        banane.x = zufällige_x_position()
        banane.y = zufällige_y_position()

    # Wenn die spieler den Rand berührt:
    if (spieler.x < 0 or
            spieler.x + spieler.breite > BREITE or
            spieler.y < 0 or
            spieler.y + spieler.höhe > HÖHE):
        print("Ende")
        print("Punkte: " + str(spieler.geschwindigkeit - 1))
        beende_spiel()


def zeichne_gitter():
    for x in range(SPALTEN + 1):
        pygame.draw.line(
            ANZEIGEFLÄCHE,
            GITTERFARBE,
            (x * GITTERGRÖSSE - 1, 0), # Startpunkt
            (x * GITTERGRÖSSE - 1, HÖHE), # Endpunkt
            2, # Liniendicke
        )
    for y in range(REIHEN + 1):
        pygame.draw.line(
            ANZEIGEFLÄCHE,
            GITTERFARBE,
            (0, y * GITTERGRÖSSE - 1), # Startpunkt
            (BREITE, y * GITTERGRÖSSE - 1), # Endpunkt
            2, # Liniendicke
        )


def punkteanzeige(punkte):
    # Erzeuge ein neues Text-Objekt
    text = pygame.font.Font('freesansbold.ttf', 32)
    # Mache aus dem Textobjekt ein Bild
    return text.render("Punkte: "+str(punkte), True, SCHWARZ)


def zeichnen(hintergrundbild, spieler, banane):
    # Wir füllen die Anzeigefläche mit weißer Farbe:
    ANZEIGEFLÄCHE.fill(WEISS)

    # blit: Bit Block Image Transfer - Bewege ein Bild (auf eine für den
    # Computer einfache Weise).
    # Zeichne den Hintergrund.
    ANZEIGEFLÄCHE.blit(hintergrundbild, (0, 0))

    # Zeichne Gitter
    zeichne_gitter()

    # Zeichne die Punkteanzeige
    ANZEIGEFLÄCHE.blit(punkteanzeige(spieler.geschwindigkeit-1), (40, 40))

    # Zeichne die Banane:
    ANZEIGEFLÄCHE.blit(banane.bild, (banane.x, banane.y))

    # Zeichne die Kazte:
    ANZEIGEFLÄCHE.blit(spieler.bild, (spieler.x, spieler.y))

    # Wir haben die Anzeigefläche bearbeitet. Nun muss der Bildschirm
    # akualisiert werden.
    pygame.display.update()


def main():
    # Um Variablen zu verändern, die außerhal der Funktion definiert wurden,
    # müssen diese als "global" gekennzeichnet werden.

    hintergrundbild, spieler, banane = initialisiere_spiel()

    # Haupt-Spiel-Schleife
    while True:
        ereigniss(spieler)
        bewege(spieler, banane)
        zeichnen(hintergrundbild, spieler, banane)

        # Jetzt müssen wir warten, bis genügend Zeit vergangen ist.
        bpsUhr.tick(BPS)


if __name__ == '__main__':
    main()
