import pygame, sys
from pygame.locals import *

# set up the colors
SCHWARZ = (  0,   0,   0)
WEISS   = (255, 255, 255)
FENSTERBREITE = 300 # size of window's width in pixels
FENSTERHÖHE   = 300 # size of windows' height in pixels
BOXGRÖSSE     = 100 # size of box height & width in pixels

# set up the window
pygame.init()
DISPLAYSURF = pygame.display.set_mode((FENSTERBREITE, FENSTERHÖHE))
pygame.display.set_caption('Tic Tac Toe')

spielbrett = ['', 'X', 'O', ' ', ' ', ' ', ' ', ' ', ' ', ' ',]


def zeichneSpielbrett(spielbrett):
    DISPLAYSURF.fill(WEISS)
    pygame.draw.line(DISPLAYSURF, SCHWARZ, (99, 0), (99, 300), 3)
    pygame.draw.line(DISPLAYSURF, SCHWARZ, (199, 0), (199, 300), 3)
    pygame.draw.line(DISPLAYSURF, SCHWARZ, (0, 99), (300, 99), 3)
    pygame.draw.line(DISPLAYSURF, SCHWARZ, (0, 199), (300, 199), 3)
    if spielbrett[1] == 'X':
        pygame.draw.line(DISPLAYSURF, SCHWARZ, (0, 299), (99, 200), 3)
        pygame.draw.line(DISPLAYSURF, SCHWARZ, (0, 200), (99, 299), 3)


def leftTopCoordsOfBox(boxx, boxy):
    # Convert board coordinates to pixel coordinates
    left = boxx * BOXGRÖSSE
    top = boxy * BOXGRÖSSE
    return (left, top)

def getBoxAtPixel(x, y):
    for boxx in range(FENSTERBREITE):
        for boxy in range(FENSTERHÖHE):
            left, top = leftTopCoordsOfBox(boxx, boxy)
            boxRect = pygame.Rect(left, top, BOXGRÖSSE, BOXGRÖSSE)
            if boxRect.collidepoint(x, y):
                return (boxx, boxy)
    return (None, None)


def drawIcon(shape, color, boxx, boxy):
    links, oben = leftTopCoordsOfBox(boxx, boxy) # get pixel coords from board coords
    # Draw the shapes


# run the game loop
while True:
    for event in pygame.event.get():
        zeichneSpielbrett(spielbrett)
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
    pygame.display.update()
