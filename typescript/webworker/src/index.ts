function main() {

    let start_button = document.createElement('button');
    let stop_button = document.createElement('button');
    let result_box = document.createElement('div');

    document.body.append(start_button);
    document.body.append(stop_button);
    document.body.append(result_box);

    let worker: Worker;

    start_button.onclick = event => {
        worker = new Worker('static/js/worker.js');

        worker.onmessage = (message) => {
            result_box.innerHTML = `${message.data}`;
        };
    };

}

window.onload = main;