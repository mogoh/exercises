# While-Schleifen laufen, bis eine Bedingung erfüllt ist.
x = 0
while x <= 3:
    print(x)
    x += 1

"""
Ausgabe:
    0
    1
    2
    3
"""

while False:
    print("Dieses Programm ist sinnlos.")

#while True:
#    print("Dieses Programm läuft ewig!")


# while-Schleifen können wir "verschachteln".
# Das bedeutet, wir schreiben eine While-Schleife in eine Whileschleife.
# Das geht beliebig oft und erfordert viel nachdenken.

