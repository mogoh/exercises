### **Welcome to the HYG star database archive.  The most current version of the database will always be found here.

For additional background details, and older versions of the database, visit  http://www.astronexus.com/hyg.

For the most current version of the applications using this database, visit http://www.astronexus.com/endeavour. **

#### Database field descriptions:


#####hygdata_v3.csv:  This is the current version (3) of the HYG stellar database.  It is similar to the version 2 (hygxyz.csv) file, but has a few updates.  The older file is now deprecated.

1. All stars now have both an epoch and equinox of 2000.0.  In v2 of the catalog, all three primary source catalogs either had or were adjusted to equinox 2000, but all 3 had different epochs, leading to small position errors at high magnifications.
2. The Flamsteed numbers now include many that were not in the _Yale Bright Star Catalog_, the
primary reference for these numbers in the original catalog.  In particular, it now contains all valid numbers listed in "Flamsteed's Missing Stars", M. Wagman, JHA xviii (1987), p 210-223.
3. Some errors in proper motions have been corrected.
4. A few additional proper names have been added.
5. For stars in Hipparcos that are known to be variable, the variable star designations have been added.  In general,
stars that were merely suspected of variability ("NSV") were excluded.



Fields in the database:

0. id: The database primary key.
1. hip: The star's ID in the Hipparcos catalog, if known.
2. hd: The star's ID in the Henry Draper catalog, if known.
3. hr: The star's ID in the Harvard Revised catalog, which is the same as its number in the Yale Bright Star Catalog.
4. gl: The star's ID in the third edition of the Gliese Catalog of Nearby Stars.
5. bf: The Bayer / Flamsteed designation, primarily from the Fifth Edition of the Yale Bright Star Catalog. This is a combination of the two designations. The Flamsteed number, if present, is given first; then a three-letter abbreviation for the Bayer Greek letter; the Bayer superscript number, if present; and finally, the three-letter constellation abbreviation. Thus Alpha Andromedae has the field value "21Alp And", and Kappa1 Sculptoris (no Flamsteed number) has "Kap1Scl".
6. proper: A common name for the star, such as "Barnard's Star" or "Sirius". I have taken these names primarily from the Hipparcos project's web site, which lists representative names for the 150 brightest stars and many of the 150 closest stars. I have added a few names to this list. Most of the additions are designations from catalogs mostly now forgotten (e.g., Lalande, Groombridge, and Gould ["G."]) except for certain nearby stars which are still best known by these designations.
7./8. ra, dec: The star's right ascension and declination, for epoch and equinox 2000.0.
9. dist: The star's distance in parsecs, the most common unit in astrometry. To convert parsecs to light years, multiply by 3.262. A value >= 10000000 indicates missing or dubious (e.g., negative) parallax data in Hipparcos.
10./11. pmra, pmdec:  The star's proper motion in right ascension and declination, in milliarcseconds per year.
12. rv:  The star's radial velocity in km/sec, where known.
13. mag: The star's apparent visual magnitude.
14. absmag: The star's absolute visual magnitude (its apparent magnitude from a distance of 10 parsecs).
15. spect: The star's spectral type, if known.
16. ci: The star's color index (blue magnitude - visual magnitude), where known.
17./18./19. x,y,z: The Cartesian coordinates of the star, in a system based on the equatorial coordinates as seen from Earth. +X is in the direction of the vernal equinox (at epoch 2000), +Z towards the north celestial pole, and +Y in the direction of R.A. 6 hours, declination 0 degrees.
20./21./22. vx,vy,vz: The Cartesian velocity components of the star, in the same coordinate system described immediately above. They are determined from the proper motion and the radial velocity (when known). The velocity unit is parsecs per year; these are small values (around 1 millionth of a parsec per year), but they enormously simplify calculations using parsecs as base units for celestial mapping.
23./24./25./26. rarad, decrad, pmrarad, prdecrad:  The positions in radians, and proper motions in radians per year.
27. bayer:  The Bayer designation as a distinct value
28. flam:  The Flamsteed number as a distinct value
29. con:  The standard constellation abbreviation
30./31./32. comp, comp\_primary, base:  Identifies a star in a multiple star system.  comp = ID of companion star, comp\_primary = ID of primary star for this component, and base = catalog ID or name for this multi-star system.  Currently only used for Gliese stars.
33. lum:  Star's luminosity as a multiple of Solar luminosity.
34. var:  Star's standard variable star designation, when known.
35./36. var\_min, var\_max:  Star's approximate magnitude range, for variables.  This value is based on the Hp magnitudes for the range in the original Hipparcos catalog, adjusted to the V magnitude scale to match the "mag" field.

