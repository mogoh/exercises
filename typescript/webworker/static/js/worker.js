"use strict";
self.onmessage = (message) => {

    //    self.close();
};

let n = 2;
while (true) {
    if (is_prime(n)) {
        self.postMessage(n);
    }
    n += 1;
}

function is_prime(n) {
    let start = 2;
    let n_sqrt = Math.sqrt(n);
    while (start <= n_sqrt) {
        if (n % start === 1) {
            return false;
        }
        start += 1;
    }
    return true;
}
//# sourceMappingURL=worker.js.map