import resolve from '@rollup/plugin-node-resolve';
import { default as importHTTP } from 'import-http/rollup';
import typescript from '@rollup/plugin-typescript';
import { terser } from 'rollup-plugin-terser';
import { nodeResolve } from '@rollup/plugin-node-resolve';


const isProduction = process.env.NODE_ENV === 'production'


const dev = {
    input: './src/ts/main.ts',
    output: {
        dir: 'temp/js/',
        // file: 'temp/bundle.js',
        format: 'esm',
        entryFileNames: '[name].js',
        sourcemap: 'inline',
    },
    plugins: [
        nodeResolve(),
        typescript(),
        resolve(),
        importHTTP(),
    ],
    watch: {
        exclude: ['node_modules/**'],
    },
}

const prod = {
    input: './src/ts/main.ts',
    output: {
        dir: 'public/js/',
        // file: 'dist/bundle.js',
        format: 'esm',
        sourcemap: false,
        compact: true,
        minifyInternalExports: true,
        entryFileNames: '[name].js',
    },
    plugins: [
        nodeResolve(),
        typescript({ inlineSourceMap: false }),
        resolve(),
        importHTTP(),
        terser(),
    ],
}

export default isProduction
    ? prod
    : dev;
