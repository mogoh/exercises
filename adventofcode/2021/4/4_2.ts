const INPUT_FILE = "./input4.txt";

const text = await Deno.readTextFile(INPUT_FILE);

const [random_numbers_, ...boards_] = text.split("\n\n");

const random_numbers = random_numbers_.split(",").map(number => parseInt(number, 10));
let boards = boards_.map(board => board.trim().split(/\s+/).map(number => parseInt(number, 10)));

function get_row(board: number[], row: number): number[] {
    return board.slice(row * 5, row * 5 + 5);
}

function get_column(board: number[], column: number): number[] {
    return board.filter((_, index) => index % 5 === column);
}

function check_board(board: number[], lucky_numbers: number[]): boolean {
    for (let i = 0; i < 5; i++) {
        const row = get_row(board, i);
        const column = get_column(board, i);
        if (row.every(number => lucky_numbers.includes(number)) || column.every(number => lucky_numbers.includes(number))) {
            return true;
        }
    }
    return false;
}

function print_board(board: number[]): void {
    for (let i = 0; i < 5; i++) {
        const row = get_row(board, i);
        console.log(row.join(" "));
    }
}

let lucky_numbers: number[] = [];
let winning_board: number[] = [];

let i = 1;
while (boards.length > 1) {
    lucky_numbers = random_numbers.slice(0, i);
    boards = boards.filter(board => !check_board(board, lucky_numbers));
    i += 1;
}

number_loop:
for (const lucky_number of random_numbers) {
    lucky_numbers.push(lucky_number);

    for (const board of boards) {
        if (check_board(board, lucky_numbers)) {
            winning_board = board;
            break number_loop;
        }
    }
}

winning_board = boards[0];

print_board(winning_board);
console.log(lucky_numbers);

let sum = 0;

winning_board.forEach(number => !lucky_numbers.includes(number) ? sum += number : null);
console.log(sum * lucky_numbers[lucky_numbers.length - 1]);
