const text = await Deno.readTextFile("./input1.txt");

const numbers = text.split("\n").map(line => parseInt(line, 10));

let counter = 0;
for (let i = 3; i < numbers.length; i++) {
    const window_a = numbers[i - 3] + numbers[i - 2] + numbers[i - 1];
    const window_b = numbers[i - 2] + numbers[i - 1] + numbers[i];
    if (window_a < window_b) {
        counter += 1;
    }
}

console.log(counter);
