class Cell {
    td_list: HTMLTableDataCellElement[] = [];
    content: string;
    x: number;
    y: number;
    z: number;

    constructor(x: number, y: number, z: number) {
        this.content = '';
        this.x = x;
        this.y = y;
        this.z = z;
    }

    get_new_td(): HTMLTableDataCellElement {
        let td = document.createElement('td');
        td.innerText = this.content;
        this.td_list.push(td);

        td.onclick = () => {
            this.content = 'gray';
            this.update_td();
        };

        return td;
    }

    update_td() {
        for (let td of this.td_list) {
            td.classList.toggle(this.content);
        }
    }
}

export class Map {
    map: any[][][];
    x_size: number;
    x_max: number;
    y_size: number;
    y_max: number;
    z_size: number;
    z_max: number;

    constructor(container: HTMLElement) {
        let size = 11;
        this.x_size = size;
        this.x_max = size - 1;
        this.y_size = size;
        this.y_max = size - 1;
        this.z_size = size;
        this.z_max = size - 1;


        this.map = new Array(this.x_size);
        for (let x = 0; x <= this.x_max; x += 1) {
            this.map[x] = new Array(this.y_size);
            for (let y = 0; y <= this.y_max; y += 1) {
                this.map[x][y] = new Array(this.z_size);

                for (let z = 0; z <= this.z_max; z += 1) {
                    this.map[x][y][z] = new Cell(x, y, z);
                }
            }
        }

        for (let i = 1; i < size; i += 1) {
            let h2 = document.createElement('h2');
            h2.innerText = `${i}`;
            container.append(h2);
            let table_row = document.createElement('div');
            table_row.classList.add('table_row');
            container.append(table_row);


            // top-down
            let top_down_table = document.createElement('table');
            for (let j = 0; j <= this.x_max; j += 1) {
                let col = document.createElement('col');
                top_down_table.append(col);
            }
            for (let y = this.y_max; y >= 0; y -= 1) {
                let tr = document.createElement('tr');
                for (let x = 0; x <= this.x_max; x += 1) {
                    if (x === 0) {
                        let td = document.createElement('td');
                        td.innerText = `y: ${y}`;
                        tr.append(td);
                    } else if (y === 0) {
                        let td = document.createElement('td');
                        td.innerText = `x: ${x}`;
                        tr.append(td);
                    } else {
                        tr.append(this.map[x][y][i].get_new_td());
                    }
                }
                top_down_table.append(tr);
            }
            table_row.append(top_down_table);

            // side
            let side_table = document.createElement('table');
            for (let j = 0; j <= this.x_max; j += 1) {
                let col = document.createElement('col');
                side_table.append(col);
            }
            for (let z = this.y_max; z >= 0; z -= 1) {
                let tr = document.createElement('tr');
                for (let x = 0; x <= this.x_max; x += 1) {
                    if (x === 0) {
                        let td = document.createElement('td');
                        td.innerText = `z: ${z}`;
                        tr.append(td);
                    } else if (z === 0) {
                        let td = document.createElement('td');
                        td.innerText = `x: ${x}`;
                        tr.append(td);
                    } else {
                        tr.append(this.map[x][i][z].get_new_td());
                    }
                }
                side_table.append(tr);
            }
            table_row.append(side_table);

            // front
            let front_table = document.createElement('table');
            for (let j = 0; j <= this.x_max; j += 1) {
                let col = document.createElement('col');
                front_table.append(col);
            }
            for (let z = this.y_max; z >= 0; z -= 1) {
                let tr = document.createElement('tr');
                for (let y = 0; y <= this.x_max; y += 1) {
                    if (y === 0) {
                        let td = document.createElement('td');
                        td.innerText = `z: ${z}`;
                        tr.append(td);
                    } else if (z === 0) {
                        let td = document.createElement('td');
                        td.innerText = `y: ${y}`;
                        tr.append(td);
                    } else {
                        tr.append(this.map[i][y][z].get_new_td());
                    }
                }
                front_table.append(tr);
            }
            table_row.append(front_table);
        }
    }
}