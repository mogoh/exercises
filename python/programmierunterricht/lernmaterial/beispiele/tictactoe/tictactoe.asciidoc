# Tic Tac Toe ##################################################################

* def zeichneSpielbrett(spielbrett):
  * Zeichne Spielbrett

* def eingabeSpielerBuchstabe():
  * Frage Spieler nach Buchstaben

* def werIstErster():
  * Wähle zufällig wer erster ist

* def macheZug(spielbrett, buchstabe, zug):
  * mache Zug

* def istGewinner(spielbrett, buchstabe):
  * Teste Gewinner

* def kopiereSpielbrett(spielbrett):
  * Kopiere Spielbrett

* def istFeldFrei(spielbrett, zug):
  * Test, ob ein Feld frei ist.

* def erhalteSpielerzug(spielbrett):
  * Frage nach Zug
  * Teste ob Zug möglich ist

* def wähleZufälligenZugAusListe(spielbrett, zügeListe):
  * Teste ob Feld frei ist
  * Wähle Zug

* def erhalteComputerZug(spielbrett, computerBuchstabe):
  * Weise Buchstaben zu
  * Prüfe ob computer gewinne kann
  * Prüfe ob Spieler gewinnen kann
  * Nimm Ecke oder Mitte oder Rand

* def istBrettVoll(spielbrett):
  * Test ob Brett voll ist

* Spieler begrüßen
* Spielbrett zeichen
* Spieler nach Buchstaben Fragen
* Auslosen, wer erster ist
* Spielbrett leeren
* Endlosschleife
* Spieler
  * Spielbrett zeichnen
  * Spieler Zug eingeben lassen
    * Zug prüfen
  * Zug durchführen
  * Auf Sieg testen
  * Auf Unentschieden testen
  * Zur KI wechseln
* KI:
  * Zug machen lassen
  * Zug druchführen
  * Auf Sieg testen
  * Auf Unentschieden testen
  * Zum Spieler wechseln
