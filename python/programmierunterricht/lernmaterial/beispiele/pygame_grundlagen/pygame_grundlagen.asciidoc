:source-highlighter: pygments
== Pygame

Pygame ist ein Modul welches uns die Eingabe und Ausgabe erleichtert und ermöglicht »Grafische Schnittstellen« (»Graphical User Interfaces« GUI) zu erstellen.
Während ohne ein weitesres Modul es nicht einfach ist GUIs zu erzeugen, nimmmt uns Pygame eine Menge Arbeit ab.

Pygame muss seperat installiert werden.

https://www.pygame.org/wiki/GettingStarted

Um zu testen, ob pygame installiert wurde, kann man im python-interpreter eintippen:
----
>>> import pygame
----
Wenn keine Fehlermeldung kommt, wurde pygame korrekt installiert.

include::leeres_pygame.asciidoc[]
<<<
include::zeichnen.asciidoc[]
