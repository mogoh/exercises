# Wir importieren die relevanten Pygame-Module und Variablen:
import sys
import os
import pygame
from pygame.locals import QUIT

# Wo befindet sich unser Python-Skript?
PFAD = os.path.dirname(os.path.realpath(__file__))

# Pygame muss zunächst initialisiert werden:
pygame.init()

# Bilder pro Sekunde
BPS = 60
# Die bpsUhr (Bilder pro Sekunde-Uhr) hilft uns dabei, dass ein Programm nicht 
# schneller läuft, als es sollte. Wir brauchen die bpsUhr später. 30 BPS
# bedeutet alle 1/60 = 0,01666... Sekunden erzeugen wir ein neues Bild. 
bpsUhr = pygame.time.Clock()

# set up the window
ANZEIGEFLÄCHE = pygame.display.set_mode((400, 300), 0, 32)
pygame.display.set_caption('Animation')

# Farbe weiß definiert als ein 3-Tupel
WEISS = (255, 255, 255)
# Wir laden ein Hintergrundbild.
hintergrundbild_pfad = os.path.join(PFAD, 'hintergrund.png')
hintergrundbild = pygame.image.load(hintergrundbild_pfad)
# Wir laden das Bild einer Katze
katzenbild_pfad = os.path.join(PFAD, 'katze.png')
katzenbild = pygame.image.load(katzenbild_pfad)
bananebild_pfad = os.path.join(PFAD, 'banane.png')
bananebild = pygame.image.load(bananebild_pfad)
katze_x = 10
katze_y = 10
# Richtung gibt an, in welche Richtung sich die Katze initial bewegen soll.
richtung_katze = 'rechts'

banane_x = 120
banane_y = 120
richtung_banane = 'rechts'

# Haupt-Spiel-Schleife
while True:
    # Wir füllen die Anzeigefläche mit weißer Farbe:
    ANZEIGEFLÄCHE.fill(WEISS)

    # Wir bewegen die Katze:
    if richtung_katze == 'rechts':
        katze_x += 2
        if katze_x >= 280:
            richtung_katze = 'runter'
    elif richtung_katze == 'runter':
        katze_y += 2
        if katze_y >= 220:
            richtung_katze = 'links'
    elif richtung_katze == 'links':
        katze_x -= 2
        if katze_x <= 10:
            richtung_katze = 'hoch'
    elif richtung_katze == 'hoch':
        katze_y -= 2
        if katze_y <= 10:
            richtung_katze = 'rechts'

    # blit: Bit Block Image Transfer - Bewege ein Bild (auf eine für den
    # Computer einfache Weise).
    # Male den Hintergrund. 
    ANZEIGEFLÄCHE.blit(hintergrundbild, (0, 0))
    # Male die Kazte:
    ANZEIGEFLÄCHE.blit(katzenbild, (katze_x, katze_y))
    
    if richtung_banane == 'rechts':
        banane_x += 2
        if banane_x >= 280:
            richtung_banane = 'runter'
    elif richtung_banane == 'runter':
        banane_y += 2
        if banane_y >= 220:
            richtung_banane = 'links'
    elif richtung_banane == 'links':
        banane_x -= 20
        if banane_x <= 10:
            richtung_banane = 'hoch'
    elif richtung_banane == 'hoch':
        banane_y -= 2
        if banane_y <= 10:
            richtung_banane = 'rechts'
    ANZEIGEFLÄCHE.blit(bananebild, (banane_x, banane_y))
    

    for ereignis in pygame.event.get():
        # Wenn der Nutzer das Programm beenden möchte:
        if ereignis.type == QUIT:
            pygame.quit()
            sys.exit()

    # Wir haben die Anzeigefläche bearbeitet. Nun muss der Bildschirm 
    # akualisiert werden.
    pygame.display.update()
    # Jetzt müssen wir warten, bis genügend Zeit vergangen ist.
    bpsUhr.tick(BPS)