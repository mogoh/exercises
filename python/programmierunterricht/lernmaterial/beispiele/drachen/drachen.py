import random
import time

def zeigeEinleitung():
    print('''
Du läufst durch ein Land voller Drachen.
Vor dir sind zwei Drachenhöhlen.
Wenn der Drache freundlich ist, gibt er dir teile von seinem Schatz.
Wenn der Drache gierig und hugrig ist, will er dich fressen.''')

def wähleHöhle():
    höhle = ''
    while höhle != '1' and höhle != '2':
        print('In welche Höhle gehst du? (1 oder 2)')
        höhle = input()

    return höhle

def prüfeHöhle(gewählteHöhle):
    print('Du näherst dich der Höhle...')
    time.sleep(2)
    print('Es ist dunkel ...')
    time.sleep(2)
    print('Ein großer Drache springt vor dich. Er öffnet sein Maul und ...')
    print()
    time.sleep(2)

    freundlicheHöhle = random.randint(1, 2)

    if gewählteHöhle == str(freundlicheHöhle):
        print('Gibt dir seinen Schatz!')
    else:
        print('verschlingt dich in einem Happs!')

spieleNochEinmal = 'ja'
while spieleNochEinmal == 'ja' or spieleNochEinmal == 'j':
    zeigeEinleitung()
    höhleNummer = wähleHöhle()
    prüfeHöhle(höhleNummer)

    print('Möchstest du noch einmal spielen? (ja oder nein)')
    spieleNochEinmal = input()