#!/usr/bin/env python3
#-*- coding:utf-8 -*- 

import random

onset = [
#Plosive
["p",1000], #p Stimmloser bilabialer Plosiv (deutsches p)
["b",1000], #b Stimmhafter bilabialer Plosiv (deutsches b)
["t",1000], #t Stimmloser alveolarer Plosiv (deutsches t)
["d",1000], #d Stimmhafter alveolarer Plosiv (deutsches d)
["ʈ",1000], #ʈ Stimmloser retroflexer Plosiv
["ɖ",1000], #ɖ Stimmhafter retroflexer Plosiv
["c",1000], #c Stimmloser palataler Plosiv
["ɟ",1000], #ɟ Stimmhafter palataler Plosiv
["k",1000], #k Stimmloser velarer Plosiv (deutsch kamm, sack, christ, fuchs)
["g",1000], #g Stimmhafter velarer Plosiv (deutsches g)
["q",1000], #q Stimmloser uvularer Plosiv
["G",1000], #G Stimmhafter uvularer Plosiv

# Frikative
["ɸ",1000], #ɸ Stimmloser bilabialer Frikativ
["β",1000], #β Stimmhafter bilabialer Frikativ
["f",1000], #f Stimmloser labiodentaler Frikativ (deutsches f, ff, v, ph)
["v",1000], #v Stimmhafter labiodentaler Frikativ
["θ",1000], #θ Stimmloser dentaler Frikativ (englisch: thanks)
["ð",1000], #ð Stimmhafter dentaler Frikativ (englisch: this)
["s",1000], #s Stimmloser alveolarer Frikativ (deutsch: ss, ß)
["z",1000], #z Stimmhafter alveolarer Frikativ (deutsch: Sage, Hase)
["ʃ",1000], #ʃ Stimmloser postalveolarer Frikativ (deutsch: Fisch, Stimme, Spät)
["ʒ",1000], #ʒ Stimmhafter postalveolarer Frikativ (deutsch: Garage, Jornal, Dschihad)
["ʂ",1000], #ʂ Stimmloser retroflexer Frikativ
["​ʐ​",1000], #ʐ Stimmhafter retroflexer Frikativ
["ç",1000], #ç Stimmloser palataler Frikativ (deutsch: ich möchte reiche mädchen)
["ʝ",1000], #ʝ Stimmhafter palataler Frikativ
["x",1000], #x Stimmloser velarer Frikativ (deutsch: lachen)
["ɣ​",1000], #ɣ​ Stimmhafter velarer Frikativ
["​χ​",1000], #χ Stimmloser uvularer Frikativ
["ʁ",1000], #ʁ Stimmhafter uvularer Frikativ (realisation des r)
["ħ",1000], #ħ Stimmloser pharyngaler Frikativ
["ʕ",1000], #ʕ Stimmhafter pharyngaler Frikativ
["h",1000], #h Stimmloser glottaler Frikativ (deutsches h)
["ɦ",1000], #ɦ Stimmhafter glottaler Frikativ

#Laterale Frikative
["ɬ",1000], #ɬ Stimmloser lateraler alveolarer Frikativ
["ɮ",1000], #ɮ Stimmhafter lateraler alveolarer Frikativ

#Nasale
["m",1000], #m Stimmhafter bilabialer Nasal (deutsches m)
["ɱ",1000], #ɱ Stimmhafter labiodentaler Nasal
["n",1000], #n Stimmhafter alveolarer Nasal (deutsches n)
["ɳ",1000], #ɳ Stimmhafter retroflexer Nasal
["ɲ",1000], #ɲ Stimmhafter palataler Nasal
["ŋ",1000], #ŋ Stimmhafter velarer Nasal (Deutsch Fink, Ding)
["N",1000], #N Stimmhafter uvularer Nasal

#Vibranten
["ʙ",1000], #ʙ Stimmhafter bilabialer Vibrant
["r",1000], #r Stimmhafter alveolarer Vibrant
["ʀ",1000], #ʀ Stimmhafter uvularer Vibrant (realisation des r)

#Taps/Flaps
["ⱱ",1000], #ⱱ Stimmhafter labiodentaler Flap
["ɾ",1000], #ɾ Stimmhafter alveolarer Tap
["ɽ",1000], #ɽ Stimmhafter retroflexer Flap

#Laterale Flaps
["ɺ",1000], #ɺ Stimmhafter lateraler alveolarer Flap

#Approximanten
["ʋ",1000], #ʋ Stimmhafter labiodentaler Approximant
["ɹ",1000], #ɹ Stimmhafter alveolarer Approximant (englisches r)
["ɻ",1000], #ɻ Stimmhafter retroflexer Approximant
["j",1000], #j Stimmhafter palataler Approximant (deutsches j)
["ɰ",1000], #ɰ Stimmhafter velarer Approximant

#Laterale Approximanten
["l",1000], #l Stimmhafter lateraler alveolarer Approximant
["ɭ",1000], #ɭ Stimmhafter lateraler retroflexer Approximant
["ʎ",1000], #ʎ Stimmhafter lateraler palataler Approximant
["ʟ",1000], #ʟ Stimmhafter lateraler velarer Approximant

#Leerer onset
["",1000]
]

coda = [
#Plosive
["p",1000], #p Stimmloser bilabialer Plosiv (deutsches p)
["b",1000], #b Stimmhafter bilabialer Plosiv (deutsches b)
["t",1000], #t Stimmloser alveolarer Plosiv (deutsches t)
["d",1000], #d Stimmhafter alveolarer Plosiv (deutsches d)
["ʈ",1000], #ʈ Stimmloser retroflexer Plosiv
["ɖ",1000], #ɖ Stimmhafter retroflexer Plosiv
["c",1000], #c Stimmloser palataler Plosiv
["ɟ",1000], #ɟ Stimmhafter palataler Plosiv
["k",1000], #k Stimmloser velarer Plosiv (deutsch kamm, sack, christ, fuchs)
["g",1000], #g Stimmhafter velarer Plosiv (deutsches g)
["q",1000], #q Stimmloser uvularer Plosiv
["G",1000], #G Stimmhafter uvularer Plosiv

# Frikative
["ɸ",1000], #ɸ Stimmloser bilabialer Frikativ
["β",1000], #β Stimmhafter bilabialer Frikativ
["f",1000], #f Stimmloser labiodentaler Frikativ (deutsches f, ff, v, ph)
["v",1000], #v Stimmhafter labiodentaler Frikativ
["θ",1000], #θ Stimmloser dentaler Frikativ (englisch: thanks)
["ð",1000], #ð Stimmhafter dentaler Frikativ (englisch: this)
["s",1000], #s Stimmloser alveolarer Frikativ (deutsch: ss, ß)
["z",1000], #z Stimmhafter alveolarer Frikativ (deutsch: Sage, Hase)
["ʃ",1000], #ʃ Stimmloser postalveolarer Frikativ (deutsch: Fisch, Stimme, Spät)
["ʒ",1000], #ʒ Stimmhafter postalveolarer Frikativ (deutsch: Garage, Jornal, Dschihad)
["ʂ",1000], #ʂ Stimmloser retroflexer Frikativ
["​ʐ​",1000], #ʐ Stimmhafter retroflexer Frikativ
["ç",1000], #ç Stimmloser palataler Frikativ (deutsch: ich möchte reiche mädchen)
["ʝ",1000], #ʝ Stimmhafter palataler Frikativ
["x",1000], #x Stimmloser velarer Frikativ (deutsch: lachen)
["ɣ​",1000], #ɣ​ Stimmhafter velarer Frikativ
["​χ​",1000], #χ Stimmloser uvularer Frikativ
["ʁ",1000], #ʁ Stimmhafter uvularer Frikativ (realisation des r)
["ħ",1000], #ħ Stimmloser pharyngaler Frikativ
["ʕ",1000], #ʕ Stimmhafter pharyngaler Frikativ
["h",1000], #h Stimmloser glottaler Frikativ (deutsches h)
["ɦ",1000], #ɦ Stimmhafter glottaler Frikativ

#Laterale Frikative
["ɬ",1000], #ɬ Stimmloser lateraler alveolarer Frikativ
["ɮ",1000], #ɮ Stimmhafter lateraler alveolarer Frikativ

#Nasale
["m",1000], #m Stimmhafter bilabialer Nasal (deutsches m)
["ɱ",1000], #ɱ Stimmhafter labiodentaler Nasal
["n",1000], #n Stimmhafter alveolarer Nasal (deutsches n)
["ɳ",1000], #ɳ Stimmhafter retroflexer Nasal
["ɲ",1000], #ɲ Stimmhafter palataler Nasal
["ŋ",1000], #ŋ Stimmhafter velarer Nasal (Deutsch Fink, Ding)
["N",1000], #N Stimmhafter uvularer Nasal

#Vibranten
["ʙ",1000], #ʙ Stimmhafter bilabialer Vibrant
["r",1000], #r Stimmhafter alveolarer Vibrant
["ʀ",1000], #ʀ Stimmhafter uvularer Vibrant (realisation des r)

#Taps/Flaps
["ⱱ",1000], #ⱱ Stimmhafter labiodentaler Flap
["ɾ",1000], #ɾ Stimmhafter alveolarer Tap
["ɽ",1000], #ɽ Stimmhafter retroflexer Flap

#Laterale Flaps
["ɺ",1000], #ɺ Stimmhafter lateraler alveolarer Flap

#Approximanten
["ʋ",1000], #ʋ Stimmhafter labiodentaler Approximant
["ɹ",1000], #ɹ Stimmhafter alveolarer Approximant (englisches r)
["ɻ",1000], #ɻ Stimmhafter retroflexer Approximant
["j",1000], #j Stimmhafter palataler Approximant (deutsches j)
["ɰ",1000], #ɰ Stimmhafter velarer Approximant

#Laterale Approximanten
["l",1000], #l Stimmhafter lateraler alveolarer Approximant
["ɭ",1000], #ɭ Stimmhafter lateraler retroflexer Approximant
["ʎ",1000], #ʎ Stimmhafter lateraler palataler Approximant
["ʟ",1000], #ʟ Stimmhafter lateraler velarer Approximant

#Leerer onset
["",1000]
]

vocals = [
["i",1000], #i Ungerundeter geschlossener Vorderzungenvokal i, ie, ih
["y",1000], #y Gerundeter geschlossener Vorderzungenvokal ü, üh, y
["ɪ",1000], #ɪ Ungerundeter zentralisierter fast geschlossener Vorderzungenvokal i (Kurzes i: mitte)
["ʏ",1000], #ʏ Gerundeter zentralisierter fast geschlossener Vorderzungenvokal ü (kurzes ü: Hündin, Mücke)
["e",1000], #e Ungerundeter halbgeschlossener Vorderzungenvokal e (beten, xenographie)
["ø",1000], #ø Gerundeter halbgeschlossener Vorderzungenvokal ö, öh (Öl, Höhle)
["ɛ",1000], #ɛ Ungerundeter halboffener Vorderzungenvokal e (nett, echt)
["œ",1000], #œ Gerundeter halboffener Vorderzungenvokal kurzes ö (röcheln)
["æ",1000], #æ Ungerundeter fast offener Vorderzungenvokal (Englisch: rat)
["a",1000], #a Ungerundeter offener Vorderzungenvokal
["ɶ",1000], #ɶ Gerundeter offener Vorderzungenvokal (Österreichisch: Seil)
["ɨ",1000], #ɨ Ungerundeter geschlossener Zentralvokal
["ʉ",1000], #ʉ Gerundeter geschlossener Zentralvokal
["ʊ",1000], #ʊ Gerundeter zentralisierter fast geschlossener Hinterzungenvokal (Kurzes u: Butter, Urteil)
["ɘ",1000], #ɘ Ungerundeter halbgeschlossener Zentralvokal
["ɵ",1000], #ɵ Gerundeter halbgeschlossener Zentralvokal
["ə",1000], #ə Mittlerer Zentralvokal (Schwa)
["ɜ",1000], #ɜ Ungerundeter halboffener Zentralvokal (Englisch: fur, her)
["ɞ",1000], #ɞ Gerundeter halboffener Zentralvokal
["ɐ",1000], #ɐ Fast offener Zentralvokal (Deutsch: Der, Tier, Kinder)
["a",1000], #a (Kein IPA-Zeichen!) Ungerundeter offener Zentralvokal (Deutsches a)
["ɯ",1000], #ɯ Ungerundeter geschlossener Hinterzungenvokal (Türkisch: ı)
["u",1000], #u Gerundeter geschlossener Hinterzungenvokal (gut)
["ɤ",1000], #ɤ Ungerundeter halbgeschlossener Hinterzungenvokal
["o",1000], #o Gerundeter halbgeschlossener Hinterzungenvokal (Bote)
["ʌ",1000], #ʌ Ungerundeter halboffener Hinterzungenvokal (Englisch: worry, cut, butter, blood)
["ɔ",1000], #ɔ Gerundeter halboffener Hinterzungenvokal o (Boxen, offen, Tonne)
["ɑ",1000], #ɑ Ungerundeter offener Hinterzungenvokal a
["ɒ",1000]  #ɒ Gerundeter offener Hinterzungenvokal (Englisch: flop)
]

"""
random.seed(a="a")
for a in onset:
    a[1] = a[1]*random.randint(1,1000)
for a in coda:
    a[1] = a[1]*random.randint(1,1000)
for a in vocals:
    a[1] = a[1]*random.randint(1,1000)
random.seed()
"""