const INPUT_FILE = "./input3.txt";
// 12 figures

const text = await Deno.readTextFile(INPUT_FILE);

const report: Array<number> = text.split("\n").map(
    line => parseInt(line, 2)
);

let generator = [...report];

for (let i = 0; i < 12; i += 1) {
    let one_counter = 0;
    let zero_counter = 0;

    for (let j = 0; j < generator.length; j += 1) {
        if (generator[j] & (1 << (11 - i))) {
            one_counter += 1;
        } else {
            zero_counter += 1;
        }
    }

    let more_ones = one_counter >= zero_counter;
    generator = generator.filter(value => {
        if ((value & (1 << (11 - i))) && more_ones) {
            return true;
        } else if (!(value & (1 << (11 - i))) && !more_ones) {
            return true;
        } else {
            return false;
        }
    })

    if (generator.length === 1) {
        break;
    }
}

let scrubber = [...report];

for (let i = 0; i < 12; i += 1) {
    let s_counter = 0;

    for (let j = 0; j < scrubber.length; j += 1) {
        if (scrubber[j] & (1 << (11 - i))) {
            s_counter += 1;
        }
    }

    let less_ones = s_counter * 2 < scrubber.length;
    scrubber = scrubber.filter(value => {
        if (value & (1 << (11 - i)) && less_ones) {
            return true;
        } else if (!(value & (1 << (11 - i))) && !less_ones) {
            return true;
        } else {
            return false;
        }
    })

    if (scrubber.length === 1) {
        break;
    }
}

console.log(`oxygen generator rating: ${generator[0]}`)
console.log(`CO2 scrubber rating: ${scrubber[0]}`)
console.log(`life support rating: ${generator[0] * scrubber[0]}`);
