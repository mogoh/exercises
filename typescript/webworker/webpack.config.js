const webpack = require('webpack');
const path = require('path');

const ROOT_PATH = path.join(__dirname);

module.exports = {
    entry: {
        index: './src/index.ts',
        worker: './src/webworker/worker.ts',
    },
    output: {
        filename: '[name].js',
        path: path.resolve(ROOT_PATH, 'static/js/'),
        publicPath: 'static/js'
    },
    devtool: 'source-map',
    devServer: {
        contentBase: ROOT_PATH,
        hot: true,
        publicPath: '/static/js/',
        port: 8000,
        watchContentBase: true
    },
    module: {
        rules: [
            {
                test: /\.css$/, use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.ts$/,
                loader: 'ts-loader',
                exclude: '/node_modules/',
                options: {
                    configFile: path.join(ROOT_PATH, 'tsconfig.json'),
                },
            },
            {
                test: /\.ts$/,
                loader: 'ts-loader',
                exclude: '/node_modules/',
                options: {
                    configFile: path.join(ROOT_PATH, 'src/webworker/tsconfig.json'),
                },
            }
        ]
    },
    performance: {
        maxEntrypointSize: 400000,
        maxAssetSize: 400000
    },
    resolve: {
        extensions: ['.ts', '.js', '.json']
    }
};
