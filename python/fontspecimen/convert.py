#!/usr/bin/env python3
#-*- coding:utf-8 -*-
import os
import fontname
from subprocess import run  

#fonts = "/home/mogoh/.local/share/fonts/"

for fontfile in sorted(os.listdir("pdf")):
    pdffile = "pdf/"+fontfile[:-3]+"pdf"
    pngfile = "png/"+fontfile[:-3]+"png"

#    fname = fontname.shortName(fonts+fontfile)[0]
    cmd =[
    "convert", 
    "-quality", "100",
    "-density", "254",
    "-flatten",
    pdffile+"[0] ",
    pngfile,
    ]
    run(" ".join(cmd), shell=True)
