import sys
from enum import Enum, auto
from os.path import join, dirname, realpath
from random import randint
from typing import List

import pygame
from pygame import Surface
from pygame.event import Event
from pygame.locals import *
from pygame.time import Clock

# Where is our python script?
PATH: str = dirname(realpath(__file__))
FPS: int = 60
WINDOW_WIDTH: int = 768
WINDOW_HEIGHT: int = 768
DISPLAY_SURFACE: Surface = None
TITLE: str = 'Foo'
GRID_SIZE: int = 64
assert WINDOW_WIDTH % GRID_SIZE == 0
assert WINDOW_HEIGHT % GRID_SIZE == 0
COLUMNS: int = int(WINDOW_WIDTH / GRID_SIZE)
ROWS: int = int(WINDOW_HEIGHT / GRID_SIZE)

# RGB
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

GRIDCOLOR = WHITE


class Direction(Enum):
    UP = auto()
    DOWN = auto()
    LEFT = auto()
    RIGHT = auto()
    HALT = auto()


class GameObject:
    def __init__(
            self,
            image=None,
            width: int = None,
            height: int = None,
            x: int = 0,
            y: int = 0,
            direction: Direction = None,
            next_direction: Direction = None,
            speed: int = None
    ):
        self.image = image
        self.width: int = width
        self.height: int = height
        self.x: int = x
        self.y: int = y
        self.direction: Direction = direction
        self.next_direction: Direction = next_direction
        self.speed: int = speed

    def draw(self, display_surface: Surface) -> None:
        display_surface.blit(self.image, (self.x, self.y))


class Grid(GameObject):
    def draw(self, display_surface: Surface) -> None:
        for x in range(COLUMNS + 1):
            pygame.draw.line(display_surface, GRIDCOLOR, (x * GRID_SIZE - 1, 0), (x * GRID_SIZE - 1, WINDOW_HEIGHT), 2)
        for y in range(ROWS + 1):
            pygame.draw.line(display_surface, GRIDCOLOR, (0, y * GRID_SIZE - 1), (WINDOW_WIDTH, y * GRID_SIZE - 1), 2)


class Game:
    player: GameObject = None
    banana: GameObject = None
    fps_clock: Clock = None
    display_surface: Surface = None
    objects: dict = {
        'BACKGROUND': [],
        'FOREGROUND': [],
    }
    points: int = 0

    def __init__(self):
        pygame.init()
        pygame.display.set_caption(TITLE)
        self.fps_clock = pygame.time.Clock()
        self.display_surface = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT), 0, 32)

        background: GameObject = GameObject(
            image=pygame.image.load(join(PATH, 'hintergrund.jpg')),
        )
        self.objects['BACKGROUND'].append(background)

        self.objects['BACKGROUND'].append(Grid())

        self.banana = GameObject(
            image=pygame.image.load(join(PATH, 'banane.png')),
            width=64,
            height=64,
        )
        self.banana.x = randint(0, COLUMNS - 1) * GRID_SIZE
        self.banana.y = randint(0, ROWS - 1) * GRID_SIZE
        self.objects['FOREGROUND'].append(self.banana)

        self.player = GameObject(
            image=pygame.image.load(join(PATH, 'katze.png')),
            width=64,
            height=64,
            direction=Direction.HALT,
            speed=1,
        )
        self.player.x = randint(0, COLUMNS - 1) * GRID_SIZE
        self.player.y = randint(0, ROWS - 1) * GRID_SIZE
        self.objects['FOREGROUND'].append(self.player)

    def event(self) -> None:
        event: List[Event]
        for event in pygame.event.get():
            if event.type == QUIT:
                exit_game()
            if event.type == KEYDOWN:
                if event.key == K_LEFT:
                    self.player.next_direction = Direction.LEFT
                elif event.key == K_RIGHT:
                    self.player.next_direction = Direction.RIGHT
                elif event.key == K_UP:
                    self.player.next_direction = Direction.UP
                elif event.key == K_DOWN:
                    self.player.next_direction = Direction.DOWN
                elif event.key == K_ESCAPE:
                    exit_game()

    def move(self) -> None:
        if self.player.x % GRID_SIZE == 0 and self.player.y % GRID_SIZE == 0:
            self.player.direction = self.player.next_direction

        if self.player.direction == Direction.RIGHT:
            self.player.x += self.player.speed
        if self.player.direction == Direction.DOWN:
            self.player.y += self.player.speed
        if self.player.direction == Direction.LEFT:
            self.player.x -= self.player.speed
        if self.player.direction == Direction.UP:
            self.player.y -= self.player.speed

        if overlapping(self.player, self.banana):
            self.player.speed *= 2
            self.points += 1
            self.banana.x = randint(0, COLUMNS - 1) * GRID_SIZE
            self.banana.y = randint(0, ROWS - 1) * GRID_SIZE

        if (self.player.x < 0 or
                self.player.x + self.player.width > WINDOW_WIDTH or
                self.player.y < 0 or
                self.player.y + self.player.height > WINDOW_HEIGHT):
            print("Ende")
            print("Punkte: " + str(self.points))
            exit_game()

    def draw(self) -> None:
        objects: List[GameObject] = self.objects['BACKGROUND'] + self.objects['FOREGROUND']
        o: GameObject
        for o in objects:
            o.draw(self.display_surface)
        pygame.display.update()

    def main(self) -> None:
        while True:
            self.event()
            self.move()
            self.draw()
            self.fps_clock.tick(FPS)


def exit_game() -> None:
    pygame.quit()
    sys.exit()


def overlapping(a: GameObject, b: GameObject) -> bool:
    if (b.x >= a.x and
            b.x + b.width <= a.x + a.width and
            b.y >= a.y and
            b.y + b.height <= a.y + a.height):
        return True
    else:
        return False


if __name__ == '__main__':
    Game().main()
