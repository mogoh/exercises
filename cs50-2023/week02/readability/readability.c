#include <cs50.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>


int count_letters(string text);
int count_words(string text);
int count_sentences(string text);


int main(void) 
{
    string s = get_string("Text: ");
    int number_of_letters = count_letters(s);
    int number_of_words = count_words(s);
    int number_of_sentences = count_sentences(s);
    float letters_per_words = ((float)number_of_letters / (float)number_of_words) * 100.0;
    float sentences_per_words = ((float)number_of_sentences / (float)number_of_words) * 100.0;
    float index = 0.0588 * letters_per_words - 0.296 * sentences_per_words - 15.8;
    printf("%i letters\n", number_of_letters);
    printf("%i words\n", number_of_words);
    printf("%i sentences\n", number_of_sentences);
    printf("Letters/Words: %f\n", letters_per_words);
    printf("Sentences/Words: %f\n", sentences_per_words);
    printf("%f\n", index);

}


int count_letters(string text) {
    int sum = 0;
    for (int i = 0; i < strlen(text); i++) {
        if (isalpha(text[i])) {
            sum++;
        }
    }
    return sum;
}

int count_words(string text) {
    int sum = 0;
    bool found_word = false;
    for (int i = 0; i < strlen(text); i++) {
        if (isalpha(text[i])) {
            found_word = true;
        }
        if (isspace(text[i]) && found_word) {
            sum++;
            found_word = false;
        }
    }
    if (found_word) {
        sum++;
    }
    return sum;
}


int count_sentences(string text) {
    int sum = 0;
    for (int i = 0; i < strlen(text); i++) {
        if (text[i] == '.' || text[i] == '!' || text[i] == '?') {
            sum++;
        }
    }
    return sum;
}