const INPUT_FILE = "./input10.txt";
// const INPUT_FILE = "./input10_test.txt";
// const INPUT_FILE = "./input10_test2.txt";
const TEXT: string = await Deno.readTextFile(INPUT_FILE);

class Task10 {
    lines: string[] = [];
    score: number[] = [];

    constructor() {
        this.parse();
    }

    parse() {
        this.lines = TEXT.split("\n");
    }

    increaseScore(stack: string[]) {
        let score = 0;
        while (stack.length > 0) {
            let top = stack.pop()!;
            score *= 5;
            switch (top) {
                case "(":
                    score += 1;
                    break;
                case "[":
                    score += 2;
                    break;
                case "{":
                    score += 3;
                    break;
                case "<":
                    score += 4;
                    break;
            }
        }
        this.score.push(score);
    }

    run() {
        console.log("Task 10");

        // for (let line of this.lines) {
        this.lines.forEach((line, line_number) => {
            line_number += 1;
            let stack: string[] = [];
            let error = false;
            for (let i = 0; i < line.length && !error; i++) {
                switch (line[i]) {
                    case "(":
                    case "[":
                    case "{":
                    case "<":
                        stack.push(line[i]);
                        break;
                    case ")":
                        if (stack.length === 0) {
                            console.log("Line:", line_number, "Error: Missing opening bracket:", line[i]);
                            stack = [];
                            error = true;
                        } else {
                            let top = stack.pop()!;
                            if (top != "(") {
                                console.log("Line:", line_number, "Error: Expected ')', got '" + top + "'");
                                stack = [];
                                error = true;
                            }
                        }
                        break;
                    case "]":
                        if (stack.length === 0) {
                            console.log("Line:", line_number, "Error: Missing opening bracket:", line[i]);
                            stack = [];
                            error = true;
                        } else {
                            let top = stack.pop()!;
                            if (top != "[") {
                                console.log("Line:", line_number, "Error: Expected ']', got '" + top + "'");
                                stack = [];
                                error = true;
                            }
                        }
                        break;
                    case "}":
                        if (stack.length === 0) {
                            console.log("Line:", line_number, "Error: Missing opening bracket:", line[i]);
                            stack = [];
                            error = true;
                        } else {
                            let top = stack.pop()!;
                            if (top != "{") {
                                console.log("Line:", line_number, "Error: Expected '}', got '" + top + "'");
                                stack = [];
                                error = true;
                            }
                        }
                        break;
                    case ">":
                        if (stack.length === 0) {
                            console.log("Line:", line_number, "Error: Missing opening bracket:", line[i]);
                            stack = [];
                            error = true;
                        } else {
                            let top = stack.pop()!;
                            if (top != "<") {
                                console.log("Line:", line_number, "Error: Expected '>', got '" + top + "'");
                                stack = [];
                                error = true;
                            }
                        }
                        break;
                }
            }
            if (stack.length > 0) {
                this.increaseScore(stack);
                console.log("Line:", line_number, "Error: Missing closing bracket(s):", stack);
            }
        });

        this.score.sort((a, b) => b - a);
        const final_score = this.score[Math.floor(this.score.length / 2)]!;
        console.log("Score:", final_score);
    }
}

const task10 = new Task10();
task10.run();
