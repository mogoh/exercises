# Wir importieren die relevanten Pygame-Module und Variablen
import pygame, sys
from pygame.locals import *

# Wir initialisieren Pygame
pygame.init()

# Wir erstellen ein neues Fenster der größe 400×300 pixel
ANZEIGEFLAECHE = pygame.display.set_mode((400, 300))

# Der Titel des Fensters
pygame.display.set_caption('Hallo Pygame-Welt!')

# Haupt-Spiel-Schleife
while True:
    # Wir arbeiten alle Ereignisse ab (Mausklicks, Tastendrücken, ...)
    for ereigniss in pygame.event.get():
        # Ist eine Ereigniss vom Typ »Beenden«?
        if ereigniss.type == QUIT:
            # Beende Pygame
            pygame.quit()
            # Beende das Programm
            sys.exit()

