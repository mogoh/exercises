# Tycho ########################################################################

* [http://discordpy.readthedocs.io/en/rewrite/](http://discordpy.readthedocs.io/en/rewrite/)
* [https://github.com/Rapptz/discord.py/tree/rewrite](https://github.com/Rapptz/discord.py/tree/rewrite)
* [https://boostlog.io/@junp1234/how-to-write-a-discord-bot-in-python-5a8e73aca7e5b7008ae1da8b](https://boostlog.io/@junp1234/how-to-write-a-discord-bot-in-python-5a8e73aca7e5b7008ae1da8b)
* [https://discordapp.com/developers/applications/me/](https://discordapp.com/developers/applications/me/)

```bash
pip3 install -U https://github.com/Rapptz/discord.py/archive/rewrite.zip
pip3 install -U "yarl<1.2"
```

## tmux ##

List Sessions:
`tmux ls`

New tmux session
`tmux new -s tsb`

Attach:
`tmux a -t tsb`

Detatch:
^B^D

## ToDo ##

* Schreibtipp
