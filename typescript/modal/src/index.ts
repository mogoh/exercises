window.onload = () => {
    let modal_button = document.getElementById('modal_button');
    if (modal_button !== null) {
        new Modal(document.body, modal_button);
    }
};


class Modal {
    currently_visible: boolean = false;
    outer_container: HTMLElement;
    constructor_element: HTMLElement;

    constructor(container: HTMLElement, constructor_element: HTMLElement) {
        this.outer_container = container;
        this.constructor_element = constructor_element;


        constructor_element.onclick = (event: Event) => {
            if (!this.currently_visible) {
                this.currently_visible = true;
                let inner_container = this.generate_modal();
                container.append(inner_container);
            }
        };
    }

    generate_modal(): HTMLElement {
        let inner_container = document.createElement('div');
        inner_container.classList.add('modal_inner_container');

        let modal_close = document.createElement('button');
        modal_close.classList.add('modal_close');
        modal_close.innerHTML = '&#x2716;';

        let modal_content = document.createElement('div');
        modal_content.classList.add('modal_content');
        modal_content.append('Modal Content.');

        inner_container.append(modal_close);
        inner_container.append(modal_content);

        let onclick = (event: Event) => {
            if (event.target === inner_container || event.target === modal_close) {
                inner_container.outerHTML = '';
                this.currently_visible = false;
            }
        };

        modal_close.onclick = onclick;
        inner_container.onclick = onclick;

        return inner_container;
    }
}
