#!/usr/bin/env python3

import markovify

class Words(markovify.NewlineText):
    DEFAULT_MAX_OVERLAP_RATIO = 0.7
    DEFAULT_MAX_OVERLAP_TOTAL = 15
    DEFAULT_TRIES = 10000

    def word_split(self, sentence):
        """
        Splits a sentence into a list of words.
        """
        return list(sentence)

    def word_join(self, words):
        """
        Re-joins a list of words into a sentence.
        """
        return "".join(words)


#text_file = "deutsch/häufigsten 10000 wörter.txt"
#text_file = "namen/de_w.txt"
text_file = "pornodarsteller.txt"
text_file = "gottheiten.txt"

with open(text_file) as f:
    text = f.read()

# Build the model.
text_model = Words(text, state_size=3)

for i in range(10):
    print(text_model.make_sentence())