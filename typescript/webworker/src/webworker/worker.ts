self.onmessage = (message: MessageEvent) => {
    let n: number = 2;

    while (true) {
        if (is_prime(n)) {
            self.postMessage(n);
        }
        n += 1;
    }
//    self.close();
};

function is_prime(n: number): boolean {
    let start = 2;
    let n_sqrt = Math.sqrt(n);
    while (start <= n_sqrt) {
        if (n % start === 1) {
            return false;
        }
        start += 1;
    }
    return true;
}