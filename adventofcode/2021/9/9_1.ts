const INPUT_FILE = "./input9.txt";
// const INPUT_FILE = "./input9_test.txt";

const text: string = await Deno.readTextFile(INPUT_FILE);
const heightmap: number[][] =
    text.split("\n")
        .map(
            line => line.split("")
                .map(
                    digit => parseInt(digit, 10)
                )
        );

function getNeighbors(x: number, y: number): number[] {
    const neighbors: number[] = [];
    if (x > 0) {
        neighbors.push(heightmap[x - 1][y]);
    }
    if (x < heightmap.length - 1) {
        neighbors.push(heightmap[x + 1][y]);
    }
    if (y > 0) {
        neighbors.push(heightmap[x][y - 1]);
    }
    if (y < heightmap[x].length - 1) {
        neighbors.push(heightmap[x][y + 1]);
    }
    return neighbors;
}

const low_points: number[] = [];

for (let i = 0; i <= heightmap.length - 1; i++) {
    for (let j = 0; j <= heightmap[i].length - 1; j++) {
        const neighbors = getNeighbors(i, j);
        const min = Math.min(...neighbors);
        if (heightmap[i][j] < min) {
            low_points.push(heightmap[i][j]);
        }
    }
}

let sum = low_points.map(p => p+1).reduce((a, b) => a + b);

console.log(sum);