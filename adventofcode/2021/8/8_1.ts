const INPUT_FILE = "./input8.txt";
// const INPUT_FILE = "./input8_test.txt";

const text: string = await Deno.readTextFile(INPUT_FILE);
const ss_encoded: string[][][] = text.split("\n").map(s => s.split(" | ").map(s => s.split(" ")));

console.log(ss_encoded);

let count1478 = 0;

ss_encoded.forEach(line => {
    line[1].forEach(digit => {
        if (digit.length === 2 || digit.length === 3 || digit.length === 4 || digit.length === 7) {
            count1478 += 1;
        }
    });
});

console.log(count1478);
