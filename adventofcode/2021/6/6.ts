const INPUT_FILE = "./input6.txt";
const DAYS: number = 256;
const text: string = await Deno.readTextFile(INPUT_FILE);
const fishes_: number[] = text.split(",").map(Number);
console.log(fishes_);

// Could be much easier with an simple array, where the index is the day and the value is the value
const fishes: Map<number, number> = new Map<number, number>([
    [0, 0],
    [1, 0],
    [2, 0],
    [3, 0],
    [4, 0],
    [5, 0],
    [6, 0],
    [7, 0],
    [8, 0],
]);

fishes_.forEach(days => fishes.set(days, fishes.get(days)! + 1));

console.log(fishes);

for (let i = 1; i <= DAYS; i += 1) {
    const new_six = fishes.get(0)!;
    const new_eight = fishes.get(0)!;
    for (let j = 0; j <= 7; j += 1) {
        fishes.set(j, fishes.get(j + 1)!);
    }
    fishes.set(6, fishes.get(6)! + new_six);
    fishes.set(8, new_eight);
    console.log(fishes);
}

let sum = 0;
fishes.forEach(count => {
    sum += count;
});

console.log('Sum: ', sum);
