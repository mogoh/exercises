const text = await Deno.readTextFile("./input1.txt");

const numbers = text.split("\n").map(line => parseInt(line, 10));

let counter = 0;
for (let i = 1; i < numbers.length; i++) {
    if (numbers[i - 1] < numbers[i]) {
        counter += 1;
    }
}

console.log(counter);
