=== Farben

Ein Pixel besteht üblicher Weise aus drei unterschiedliche farbigen Lichter: Rot, Grün und Blau.
Mischen wir diese Lichter, können wir alle möglichen Farben erhalten.
